// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32857,y:32712,varname:node_3138,prsc:2|normal-49-RGB,custl-799-OUT;n:type:ShaderForge.SFN_ViewVector,id:1690,x:31101,y:32838,varname:node_1690,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:49,x:32650,y:32507,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_49,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bbab0a6f7bae9cf42bf057d8ee2755f6,ntxv:3,isnm:True;n:type:ShaderForge.SFN_LightVector,id:4201,x:31101,y:32964,varname:node_4201,prsc:2;n:type:ShaderForge.SFN_LightColor,id:3249,x:32378,y:33169,varname:node_3249,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:2889,x:32378,y:33295,varname:node_2889,prsc:2;n:type:ShaderForge.SFN_Multiply,id:799,x:32582,y:33041,varname:node_799,prsc:2|A-5942-OUT,B-3249-RGB,C-2889-OUT;n:type:ShaderForge.SFN_LightVector,id:8482,x:31476,y:32637,varname:node_8482,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:1733,x:31476,y:32783,prsc:2,pt:True;n:type:ShaderForge.SFN_Dot,id:8925,x:31668,y:32724,varname:node_8925,prsc:2,dt:1|A-8482-OUT,B-1733-OUT;n:type:ShaderForge.SFN_Tex2d,id:7798,x:31668,y:32550,ptovrint:False,ptlb:Diffues,ptin:_Diffues,varname:node_7798,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b66bceaf0cc0ace4e9bdc92f14bba709,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:9390,x:31915,y:32724,varname:node_9390,prsc:2|A-7798-RGB,B-8925-OUT;n:type:ShaderForge.SFN_Dot,id:7127,x:31668,y:32884,varname:node_7127,prsc:2,dt:1|A-1733-OUT,B-3682-OUT;n:type:ShaderForge.SFN_Power,id:3771,x:31915,y:32884,varname:node_3771,prsc:2|VAL-7127-OUT,EXP-9150-OUT;n:type:ShaderForge.SFN_Slider,id:6460,x:31129,y:33120,ptovrint:False,ptlb:Glossiness,ptin:_Glossiness,varname:node_6460,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3919459,max:1;n:type:ShaderForge.SFN_Multiply,id:8475,x:31476,y:33100,varname:node_8475,prsc:2|A-6460-OUT,B-4587-OUT;n:type:ShaderForge.SFN_Vector1,id:4587,x:31286,y:33201,varname:node_4587,prsc:2,v1:8;n:type:ShaderForge.SFN_Add,id:5942,x:32316,y:32866,varname:node_5942,prsc:2|A-9390-OUT,B-7528-OUT;n:type:ShaderForge.SFN_Exp,id:9150,x:31668,y:33036,varname:node_9150,prsc:2,et:1|IN-8475-OUT;n:type:ShaderForge.SFN_Multiply,id:7528,x:32119,y:32884,varname:node_7528,prsc:2|A-3771-OUT,B-9514-OUT;n:type:ShaderForge.SFN_Slider,id:9514,x:31648,y:33217,ptovrint:False,ptlb:Specularity,ptin:_Specularity,varname:node_9514,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Add,id:6641,x:31293,y:32964,varname:node_6641,prsc:2|A-1690-OUT,B-4201-OUT;n:type:ShaderForge.SFN_Normalize,id:3682,x:31476,y:32964,cmnt:Same as HalfDir,varname:node_3682,prsc:2|IN-6641-OUT;proporder:49-7798-6460-9514;pass:END;sub:END;*/

Shader "ShaderExamples/Blinn-Phong" {
    Properties {
        _Normal ("Normal", 2D) = "bump" {}
        _Diffues ("Diffues", 2D) = "white" {}
        _Glossiness ("Glossiness", Range(0, 1)) = 0.3919459
        _Specularity ("Specularity", Range(0, 1)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 3.0
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Diffues; uniform float4 _Diffues_ST;
            uniform float _Glossiness;
            uniform float _Specularity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Diffues_var = tex2D(_Diffues,TRANSFORM_TEX(i.uv0, _Diffues));
                float3 finalColor = (((_Diffues_var.rgb*max(0,dot(lightDirection,normalDirection)))+(pow(max(0,dot(normalDirection,normalize((viewDirection+lightDirection)))),exp2((_Glossiness*8.0)))*_Specularity))*_LightColor0.rgb*attenuation);
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 
            #pragma target 3.0
            uniform sampler2D _Normal; uniform float4 _Normal_ST;
            uniform sampler2D _Diffues; uniform float4 _Diffues_ST;
            uniform float _Glossiness;
            uniform float _Specularity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                LIGHTING_COORDS(5,6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _Normal_var = UnpackNormal(tex2D(_Normal,TRANSFORM_TEX(i.uv0, _Normal)));
                float3 normalLocal = _Normal_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float4 _Diffues_var = tex2D(_Diffues,TRANSFORM_TEX(i.uv0, _Diffues));
                float3 finalColor = (((_Diffues_var.rgb*max(0,dot(lightDirection,normalDirection)))+(pow(max(0,dot(normalDirection,normalize((viewDirection+lightDirection)))),exp2((_Glossiness*8.0)))*_Specularity))*_LightColor0.rgb*attenuation);
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
