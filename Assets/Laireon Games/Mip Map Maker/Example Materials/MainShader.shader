// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Laircraft/Main"
{
	Properties
	{
		_Tex("Tex", 2DArray) = "" {}
		_SliceRange("Slices", Range(0,16)) = 6
		_UVScale("UVScale", Float) = 1.0
		_Color("_Colour", Color) = (1,1,1,1)
	}
		SubShader
		{
			Pass
		{
			CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
			// to use texture arrays we need to target DX10/OpenGLES3 which
			// is shader model 3.5 minimum
	#pragma target 3.5

	#include "UnityCG.cginc"

		struct v2f
		{
			float3 uv : TEXCOORD0;//note the float3 instead of float2!
			float4 vertex : SV_POSITION;
		};

		float _SliceRange;
		float _UVScale;
		float4 _Color;
		float _Cutoff;

		v2f vert(appdata_full v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv.xy = (v.texcoord.xy) * _UVScale;
			o.uv.z = v.texcoord.z;//this is reading in the texture index from the UV data
			return o;
		}

		UNITY_DECLARE_TEX2DARRAY(_Tex);//this line is pretty dam important!!

		half4 frag(v2f i) : SV_Target
		{
			return UNITY_SAMPLE_TEX2DARRAY(_Tex, i.uv) * _Color;//this is the magic line. Note that your uv.z now translates which texture in the array
		}
			ENDCG
		}
		}
}