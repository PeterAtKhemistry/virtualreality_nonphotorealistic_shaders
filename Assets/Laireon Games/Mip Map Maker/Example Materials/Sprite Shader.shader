Shader "Custom Mips/Sprites"
{
	Properties
	{
		_Tex("Tex", 2DArray) = "" {}
		_SpriteIndex("Sprite Index", Int) = 1.0
		_Color("_Colour", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off

		Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		// to use texture arrays we need to target DX10/OpenGLES3 which
			// is shader model 3.5 minimum
	#pragma target 3.5

	#include "UnityCG.cginc"

		struct v2f
		{
			float3 uv : TEXCOORD0;//note the float3 instead of float2!
			float4 vertex : SV_POSITION;
		};

		int _SpriteIndex;
		float4 _Color;
		float _Cutoff;

		v2f vert(appdata_full v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv.xy = (v.texcoord.xy);
			o.uv.z = _SpriteIndex;//this is reading in the texture index from the UV data
			return o;
		}

		UNITY_DECLARE_TEX2DARRAY(_Tex);//this line is pretty dam important!!

		half4 frag(v2f i) : SV_Target
		{
			return UNITY_SAMPLE_TEX2DARRAY(_Tex, i.uv) * _Color;//this is the magic line. Note that your uv.z now translates which texture in the array
		}
			ENDCG
		}
		}
}