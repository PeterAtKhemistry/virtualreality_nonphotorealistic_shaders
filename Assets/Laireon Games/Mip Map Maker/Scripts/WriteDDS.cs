﻿using System.IO;
using System.Text;
using UnityEngine;

namespace MipMapMaker
{
    public static class WriteDDS
    {
        #region DDSStruct
        private struct DDSStruct
        {
            public uint size;
            public uint flags;
            public uint height;
            public uint width;
            public uint sizeorpitch;
            public uint depth;
            public uint mipmapcount;
            public uint alphabitdepth;
        }
        #endregion

        #region Constants
        const long DDS_CAPS = 0x00000001L;
        const long DDS_HEIGHT = 0x00000002L;
        const long DDS_WIDTH = 0x00000004L;

        const long DDS_RGB = 0x00000040L;
        const long DDS_PIXELFORMAT = 0x00001000L;

        const long DDS_LUMINANCE = 0x00020000L;

        const long DDS_ALPHAPIXELS = 0x00000001L;
        const long DDS_ALPHA = 0x00000002L;
        const long DDS_FOURCC = 0x00000004L;
        const long DDS_PITCH = 0x00000008L;
        const long DDS_COMPLEX = 0x00000008L;
        const long DDS_TEXTURE = 0x00001000L;
        const long DDS_MIPMAPCOUNT = 0x00020000L;
        const long DDS_LINEARSIZE = 0x00080000L;
        const long DDS_VOLUME = 0x00200000L;
        const long DDS_MIPMAP = 0x00400000L;
        const long DDS_DEPTH = 0x00800000L;

        const long DDS_CUBEMAP = 0x00000200L;
        const long DDS_CUBEMAP_POSITIVEX = 0x00000400L;
        const long DDS_CUBEMAP_NEGATIVEX = 0x00000800L;
        const long DDS_CUBEMAP_POSITIVEY = 0x00001000L;
        const long DDS_CUBEMAP_NEGATIVEY = 0x00002000L;
        const long DDS_CUBEMAP_POSITIVEZ = 0x00004000L;
        const long DDS_CUBEMAP_NEGATIVEZ = 0x00008000L;

        //const int DXTC_FORMAT = 0x0705;
        //const int DXT1 = 0x0706;
        //const int DXT2 = 0x0707;
        //const int DXT3 = 0x0708;
        //const int DXT4 = 0x0709;
        //const int DXT5 = 0x070A;
        //const int DXT_NO_COMP = 0x070B;
        //const int KEEP_DXTC_DATA = 0x070C;
        //const int DXTC_DATA_FORMAT = 0x070D;
        //const int ThreeDC = 0x070E;
        //const int RXGB = 0x070F;
        //const int ATI1N = 0x0710;
        //const int DXT1A = 0x0711;
        #endregion

        public static void CreateFile(string fileName, Texture2D[] textures)
        {
            DDSStruct header = new DDSStruct();
            header.width = (uint)textures[0].width;
            header.height = (uint)textures[0].height;
            header.mipmapcount = (uint)textures.Length;
            header.depth = 1;

            MemoryStream stream = new MemoryStream(textures[0].width * textures[0].height);//TODO make this more realistic
            BinaryWriter writer = new BinaryWriter(stream);

            WriteHeader(writer, header);

            Color32[] colours;
            Color32[] invertedColours;

            for(int i = 0; i < textures.Length; i++)//loop for each image or mip map
            {
                colours = textures[i].GetPixels32();//grab old pixel data
                invertedColours = new Color32[colours.Length];//create space for the inverted data, dds inverts pixels in the Y axis

                for(int y = 0; y < textures[i].height; y++)
                    for(int x = 0; x < textures[i].width; x++)
                        invertedColours[y * textures[i].width + x] = colours[(textures[i].height - y - 1) * textures[i].width + x];

                Texture2D temp = new Texture2D(textures[i].width, textures[i].height, TextureFormat.BGRA32, false);//convert the colours into another format for the DDS file
                temp.SetPixels32(invertedColours);
                temp.Apply();

                writer.Write(temp.GetRawTextureData());
            }

            writer.Close();
            stream.Close();

            System.IO.File.WriteAllBytes(fileName, stream.ToArray());
        }

        static bool WriteHeader(BinaryWriter writer, DDSStruct header)
        {
            long i, Flags1 = 0, linearSize, ddsCaps1 = 0, BlockSize, ddsCaps2 = 0;

            Flags1 |= DDS_LINEARSIZE | DDS_MIPMAPCOUNT | DDS_WIDTH | DDS_HEIGHT | DDS_CAPS | DDS_PIXELFORMAT;

            if(header.depth > 1)
                Flags1 |= DDS_DEPTH;

            //if(DXTCFormat == DXT2)
            //    DXTCFormat = DXT3;
            //else if(DXTCFormat == DXT4)
            //    DXTCFormat = DXT5;

            writer.Write(Encoding.ASCII.GetBytes("DDS "));
            writer.Write(124);//Size1
            writer.Write((uint)Flags1);// Flags1

            writer.Write(header.height);
            writer.Write(header.width);

            //if(DXTCFormat == DXT1 || DXTCFormat == DXT1A || DXTCFormat == ATI1N)
            //    BlockSize = 8;
            //else
            BlockSize = 16;

            linearSize = (((header.width + 3) / 4) * ((header.height + 3) / 4)) * BlockSize * header.depth;

            writer.Write((uint)linearSize);

            if(header.depth > 1)
            {
                writer.Write(header.depth);// Depth
                ddsCaps2 |= DDS_VOLUME;
            }
            else
                writer.Write(0);// Depth

            writer.Write(header.mipmapcount);

            writer.Write(0);         // AlphaBitDepth

            for(i = 0; i < 10; i++)
                writer.Write(0);

            writer.Write(32);// Size2
            writer.Write(65);// (uint)Flags2); // Flags2
            writer.Write(0);// FourCC
            writer.Write(32);// RGBBitCount
            writer.Write(16711680u); // RBitMask
            writer.Write(65280u); // GBitMask
            writer.Write(255u); // BBitMask
            writer.Write(4278190080u);// RGBAlphaBitMask

            ddsCaps1 |= DDS_TEXTURE;

            if(header.mipmapcount > 0)
                ddsCaps1 |= DDS_MIPMAP | DDS_COMPLEX;

            writer.Write(0);// (uint)ddsCaps1);  // ddsCaps1
            writer.Write(0);// (uint)ddsCaps1);// ddsCaps2
            writer.Write(0);// ddsCaps3
            writer.Write(4286743175u);// ddsCaps4
            writer.Write(4286940553u);// TextureStage

            return true;
        }
    }
}