using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using LaireonFramework;

namespace MipMapMaker
{
    public class DDSAtlasEditor : EditorWindow
    {
        const float ImagePreviewSize = 128;

        string fileName = "";

        public int maxAtlasSize = 2048;
        float atlasScale = 0.25f;//this is the preview size
        public int atlasPadding = 2;

        float imageSize;
        bool bleedEdges = true, padEdges = true;

        int columnCount = 1;

        Texture2D[] outputMipMaps;

        bool outputDDS;
        public Texture2D[] sourceImages;
        public float[] sourceAlpha;
        Rect startingPoint;

        public Texture2D inputDDSFile;

        Vector2 scrollView;

        [MenuItem("Tools/DDS/Create DDS Atlas")]
        static void Init()
        {
            DDSAtlasEditor window = (DDSAtlasEditor)GetWindow(typeof(DDSAtlasEditor), false, "DDS Atlas Maker");
            window.Show();
        }

        void Awake()
        {
            if(EditorPrefs.HasKey("maxAtlasSize"))
                maxAtlasSize = EditorPrefs.GetInt("maxAtlasSize");//read preferences

            if(EditorPrefs.HasKey("atlasPadding"))
                atlasPadding = EditorPrefs.GetInt("atlasPadding");//read preferences

            if(EditorPrefs.HasKey("atlasScale"))
                atlasScale = EditorPrefs.GetFloat("atlasScale");
        }

        public void OnGUI()
        {
            if(sourceImages == null)
            {
                sourceImages = new Texture2D[1];
                sourceAlpha = new float[1];
                sourceAlpha[0] = 1;
            }

            if(sourceImages[sourceImages.Length - 1] != null)//if a new image has been added
            {
                System.Array.Resize<Texture2D>(ref sourceImages, sourceImages.Length + 1);//add a new slot
                System.Array.Resize<float>(ref sourceAlpha, sourceAlpha.Length + 1);//add a new slot

                sourceAlpha[sourceAlpha.Length - 1] = 1;
            }
            else if(sourceImages.Length > 2 && sourceImages[sourceImages.Length - 2] == null)//the last one is null, and the length is > 1
            {
                System.Array.Resize<Texture2D>(ref sourceImages, sourceImages.Length - 1);//remove the now empty slot
                System.Array.Resize<float>(ref sourceAlpha, sourceAlpha.Length - 1);//remove the now empty slot
            }

            scrollView = EditorGUILayout.BeginScrollView(scrollView);

            GUILayout.Space(10);

            #region Source Input
            using(new Horizontal())
            {
                using(new Vertical())
                {
                    using(new FixedWidthLabel("Max Atlas Size (Pixels)"))
                    {
                        EditorGUI.BeginChangeCheck();

                        maxAtlasSize = EditorGUILayout.IntField(maxAtlasSize, GUILayout.MaxWidth(40));

                        maxAtlasSize = Mathf.Max(4, maxAtlasSize);

                        if(EditorGUI.EndChangeCheck())
                            EditorPrefs.SetInt("maxAtlasSize", maxAtlasSize);
                    }

                    if(!Mathf.IsPowerOfTwo(maxAtlasSize))
                        using(new FixedWidthLabel("Atlas should be a power of 2!")) { }
                }

                using(new FixedWidthLabel("Padding"))
                {
                    EditorGUI.BeginChangeCheck();

                    atlasPadding = EditorGUILayout.IntField(atlasPadding, GUILayout.MaxWidth(40));

                    if(EditorGUI.EndChangeCheck())
                        EditorPrefs.SetInt("atlasPadding", atlasPadding);
                }

                using(new FixedWidthLabel("Atlas Preview Scale"))
                {
                    EditorGUI.BeginChangeCheck();

                    atlasScale = EditorGUILayout.Slider(atlasScale, 0.1f, 1f, GUILayout.MaxWidth(150));

                    if(EditorGUI.EndChangeCheck())
                        EditorPrefs.SetFloat("atlasScale", atlasScale);
                }
            }

            GUILayout.Space(10);

            using(new Horizontal())
            {
                #region Creating Atlas
                using(new Vertical())
                {
                    EditorGUILayout.LabelField("Source Textures");

                    #region Load From Selection Button
                    GUI.enabled = Selection.assetGUIDs != null && Selection.assetGUIDs.Length > 0;//if the user is selecting something in the project view

                    if(GUILayout.Button("Load From Selection", GUILayout.MaxWidth(150)))
                    {
                        sourceImages = new Texture2D[Selection.assetGUIDs.Length];
                        sourceAlpha = new float[sourceImages.Length];

                        for(int i = 0; i < Selection.assetGUIDs.Length; i++)
                        {
                            sourceImages[i] = (Texture2D)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[i]), typeof(Texture2D));
                            sourceAlpha[i] = 1;
                        }
                    }

                    GUI.enabled = true;
                    #endregion

                    bool oddNumberedEnding = false;

                    using(new Horizontal())
                        for(int i = 0; i < sourceImages.Length; i++)
                            using(new LoopingVerticle(i, columnCount, sourceImages.Length - 1, 0))
                            {
                                using(new Horizontal())
                                {
                                    int currentIndex;

                                    if(!oddNumberedEnding)
                                    {
                                        currentIndex = columnCount - (i % columnCount + 1);

                                        currentIndex += (i / columnCount) * columnCount;

                                        if(currentIndex >= sourceImages.Length)
                                        {
                                            currentIndex = sourceImages.Length - 1 - (i % columnCount);
                                            oddNumberedEnding = true;
                                        }
                                    }
                                    else
                                        currentIndex = sourceImages.Length - 1 - (i % columnCount);

                                    sourceImages[currentIndex] = (Texture2D)EditorGUILayout.ObjectField(sourceImages[currentIndex], typeof(Texture2D), false, GUILayout.MaxWidth(150));

                                    using(new FixedWidthLabel("Alpha"))
                                        sourceAlpha[currentIndex] = EditorGUILayout.FloatField(sourceAlpha[currentIndex], GUILayout.MaxWidth(50));
                                }

                                GUILayout.Space(5);
                            }
                }
                #endregion
            }
            #endregion

            columnCount = sourceImages[0] == null ? 8 : Mathf.Max(1, maxAtlasSize / (sourceImages[0].height + atlasPadding * 2));
            imageSize = (ImagePreviewSize + atlasPadding * 2) * atlasScale;

            #region Atlas Preview
            EditorGUILayout.LabelField("Atlas Preview");

            startingPoint = GUILayoutUtility.GetRect(0, imageSize * columnCount);

            for(int i = sourceImages.Length - 1; i > -1; i--)
                if(sourceImages[i] != null)
                    GUI.DrawTextureWithTexCoords(CalculateDestination(i), sourceImages[i], new Rect(0, 0, 1, 1));

            EditorGUILayout.LabelField("Please note Unity won't always respect this given order when packing an atlas!");
            #endregion

            EditorGUILayout.EndScrollView();

            using(new FixedWidthLabel("File Name:"))
                fileName = EditorGUILayout.TextField(fileName, GUILayout.MaxWidth(150));

            using(new Horizontal())
            using(new DisableUI(fileName.Length == 0))
            {
                if(GUILayout.Button("Create DDS Atlas", GUILayout.MaxWidth(150)))
                    CreateAtlas();

                using(new FixedWidthLabel("Edge Bleeding"))
                    bleedEdges = EditorGUILayout.Toggle(GUIContent.none, bleedEdges);

                using(new FixedWidthLabel("Pad Sides"))
                    padEdges = EditorGUILayout.Toggle(GUIContent.none, padEdges);

                GUILayout.FlexibleSpace();
            }

            Repaint();//force things to update a little more often
        }

        Rect CalculateDestination(int index)
        {
            Rect destination = new Rect(startingPoint.x, startingPoint.y, 0, 0);

            destination.y += (columnCount - (index % columnCount) - 1) * imageSize;
            destination.x += imageSize * (index / columnCount);

            destination.width = imageSize - atlasPadding * 2 * atlasScale;
            destination.height = imageSize - atlasPadding * 2 * atlasScale;

            return destination;
        }

        void CreateAtlas()
        {
            Texture2D baseImage = new Texture2D(0, 0, TextureFormat.ARGB32, false);

            Texture2D[] atlasTextures = new Texture2D[sourceAlpha.Length];//make a temporary copy of our textures, so we dont modify the base images
            Color[] pixels;

            for(int i = 0; i < sourceAlpha.Length; i++)
                if(sourceImages[i] != null)
                {
                    atlasTextures[i] = new Texture2D(sourceImages[i].width, sourceImages[i].height);

                    pixels = sourceImages[i].GetPixels();

                    if(sourceAlpha[i] != 1)//if we are applying alpha
                        for(int ii = 0; ii < pixels.Length; ii++)
                            pixels[ii] = new Color(pixels[ii].r, pixels[ii].g, pixels[ii].b, pixels[ii].a * sourceAlpha[i]);//apply the change

                    atlasTextures[i].SetPixels(pixels);
                    atlasTextures[i].Apply();
                }

            baseImage.PackTextures(atlasTextures, atlasPadding, maxAtlasSize);

            int width = baseImage.width;
            int height = baseImage.height;

            Texture2D outputImage = new Texture2D(baseImage.width, baseImage.height, TextureFormat.ARGB32, true);

            pixels = baseImage.GetPixels();

            if(padEdges)
            {
                Color[] blankPixels = new Color[pixels.Length];

                for(int i = 0; i < blankPixels.Length; i++)
                    blankPixels[i] = new Color(0, 0, 0, 0);

                outputImage.SetPixels(blankPixels);//fill the entire image

                pixels = baseImage.GetPixels(0, 0, width - atlasPadding, height - atlasPadding, 0);

                outputImage.SetPixels(atlasPadding, atlasPadding, width - atlasPadding, height - atlasPadding, pixels, 0);
                outputImage.Apply();

                pixels = outputImage.GetPixels();
            }

            if(bleedEdges)
            {
                for(int i = 0; i < atlasPadding; i++)//run this for each pixel of padding
                {
                    Color[] inputPixels = (Color[])pixels.Clone();

                    for(int y = 0; y < baseImage.height; y++)//loop for all pixels
                        for(int x = 0; x < baseImage.width; x++)
                            if(pixels[y * baseImage.width + x].a == 0)//if a blank pixel
                                searchNeighbours(x, y, ref inputPixels, ref pixels, baseImage.width, baseImage.height);//search for the colour data in the neighbour. This bleeds the pixel data to prevent seeing ugly borders

                }
            }

            outputImage.SetPixels(pixels);

            outputImage.Apply(true);

            GenerateMipMapData(outputImage);

            string fileName = FilePath(this.fileName);

            WriteDDS.CreateFile(fileName, outputMipMaps);

            Debug.Log("Created: " + fileName);

            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }

        void searchNeighbours(int x, int y, ref Color[] input, ref Color[] output, int width, int height)
        {
            for(int y2 = -1; y2 < 2; y2++)//look around that pixel
                for(int x2 = -1; x2 < 2; x2++)
                {
                    int newX = x + x2;

                    if(newX > 0 && newX < width)
                    {
                        int newY = y + y2;

                        if(newY > 0 && newY < height)
                        {
                            if(input[newY * width + newX].a > 0)//if there is colour
                            {
                                output[y * width + x] = input[newY * width + newX];//copy the value
                                return;
                            }
                        }
                    }
                }
        }

        void GenerateMipMapData(Texture2D input)
        {
            outputMipMaps = new Texture2D[input.mipmapCount];

            Color32[] pixels;

            for(int i = 0; i < outputMipMaps.Length; i++)
                if(input.width >> i > 0)//if there are pixels!
                {
                    pixels = input.GetPixels32(i);//read pixels from the mip map

                    outputMipMaps[i] = new Texture2D(input.width >> i, input.height >> i, input.format, false);
                    outputMipMaps[i].SetPixels32(pixels);
                    outputMipMaps[i].Apply();
                }

            while(outputMipMaps[outputMipMaps.Length - 1] == null)
                System.Array.Resize<Texture2D>(ref outputMipMaps, outputMipMaps.Length - 1);
        }

        /// <summary>
        /// This is for creating a file in the system manually
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string FilePath(string name)
        {
            if(!Directory.Exists(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath)))
                Directory.CreateDirectory(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath));

            return string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output/{1}.dds", Application.dataPath, name);
        }

        /// <summary>
        /// this is for creating a file within the resources folder with Unity
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string AssetPath(string name)
        {
            if(!Directory.Exists(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath)))
                Directory.CreateDirectory(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath));

            return string.Format("Assets/Laireon Games/Mip Map Maker/Resources/Output/{0}.asset", name);
        }
    }
}