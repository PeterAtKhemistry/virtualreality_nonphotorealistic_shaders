﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.SceneManagement;

public class VRSceneSetup : MonoBehaviour
{
    public int VRDeviceToLoad = 1;


	// Use this for initialization
	void Start ()
    {
        UnityEngine.XR.XRSettings.enabled = true;

        StartCoroutine(LoadDevice(UnityEngine.XR.XRSettings.supportedDevices[VRDeviceToLoad]));
    }

    IEnumerator LoadDevice(string newDevice)
    {
        if (newDevice != "None")
        {
            yield return null;
        }

        UnityEngine.XR.XRSettings.LoadDeviceByName(newDevice);

        if (newDevice == "None")
        {
            yield return new WaitForEndOfFrame();

        }
        else
        {
            yield return null;
        }
        if (newDevice == "None")
        {
            UnityEngine.XR.XRSettings.enabled = false;
        }
        else
        {
            UnityEngine.XR.XRSettings.enabled = true;
        }

        yield return null;

        Camera.main.ResetAspect();
        Camera.main.fieldOfView = 60;
        LockScreenOrientation();
        Debug.Log("Current VR Device is " + UnityEngine.XR.XRSettings.loadedDeviceName + ". VR is enabled: " + UnityEngine.XR.XRSettings.enabled + ". Camera FOV: + " + Camera.main.fieldOfView + ". Camera Aspect: " + Camera.main.aspect);

    }

    void LockScreenOrientation()
    {
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToLandscapeLeft = false;
        //Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;

        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void LoadNewScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            LoadNewScene(0);
        }
    }
}
