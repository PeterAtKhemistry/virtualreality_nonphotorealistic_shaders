﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpEyeColors : MonoBehaviour
{
    public Color leftColor, rightColor;

    public float lerpSpeed;
    float offset;
    float currentLerpAmount;

    Material myMaterial;

	// Use this for initialization
	void Start ()
    {
        offset = Random.value * 2;

        lerpSpeed += (Random.value - .5f);

        myMaterial = GetComponent<Renderer>().material;

        if (!myMaterial.HasProperty("_LeftEyeColor"))
        {
            Debug.LogError("Wrong Material");
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        currentLerpAmount = Mathf.PingPong((Time.time + offset) / lerpSpeed, 1);

        myMaterial.SetColor("_LeftEyeColor", Color.Lerp(rightColor, leftColor, currentLerpAmount));
        myMaterial.SetColor("_RightEyeColor", Color.Lerp(leftColor, rightColor, currentLerpAmount));
    }
}
