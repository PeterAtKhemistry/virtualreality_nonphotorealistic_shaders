// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:0,x:34330,y:31982,varname:node_0,prsc:2|emission-3314-OUT,olwid-255-OUT;n:type:ShaderForge.SFN_Dot,id:40,x:32531,y:32701,varname:node_40,prsc:2,dt:1|A-42-OUT,B-41-OUT;n:type:ShaderForge.SFN_NormalVector,id:41,x:32322,y:32795,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:42,x:32322,y:32674,varname:node_42,prsc:2;n:type:ShaderForge.SFN_Dot,id:52,x:32531,y:32874,varname:node_52,prsc:2,dt:1|A-41-OUT,B-4413-OUT;n:type:ShaderForge.SFN_Power,id:58,x:32733,y:32974,cmnt:Specular Light,varname:node_58,prsc:2|VAL-52-OUT,EXP-244-OUT;n:type:ShaderForge.SFN_ValueProperty,id:216,x:32733,y:32874,ptovrint:False,ptlb:Bands,ptin:_Bands,varname:_Bands,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Slider,id:239,x:31584,y:33042,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Gloss,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3058287,max:1;n:type:ShaderForge.SFN_Add,id:240,x:32322,y:33091,varname:node_240,prsc:2|A-242-OUT,B-241-OUT;n:type:ShaderForge.SFN_Vector1,id:241,x:32154,y:33179,varname:node_241,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:242,x:32154,y:33029,varname:node_242,prsc:2|A-239-OUT,B-243-OUT;n:type:ShaderForge.SFN_Vector1,id:243,x:31741,y:33112,varname:node_243,prsc:2,v1:10;n:type:ShaderForge.SFN_Exp,id:244,x:32493,y:33091,varname:node_244,prsc:2,et:1|IN-240-OUT;n:type:ShaderForge.SFN_Vector1,id:255,x:34139,y:32368,varname:node_255,prsc:2,v1:0.05;n:type:ShaderForge.SFN_Posterize,id:264,x:32968,y:32795,varname:node_264,prsc:2|IN-40-OUT,STPS-216-OUT;n:type:ShaderForge.SFN_Posterize,id:265,x:32968,y:32926,varname:node_265,prsc:2|IN-58-OUT,STPS-216-OUT;n:type:ShaderForge.SFN_ViewVector,id:6727,x:31914,y:32732,varname:node_6727,prsc:2;n:type:ShaderForge.SFN_LightVector,id:9823,x:31914,y:32888,varname:node_9823,prsc:2;n:type:ShaderForge.SFN_Add,id:7067,x:32154,y:32888,varname:node_7067,prsc:2|A-6727-OUT,B-9823-OUT;n:type:ShaderForge.SFN_Normalize,id:4413,x:32322,y:32942,varname:node_4413,prsc:2|IN-7067-OUT;n:type:ShaderForge.SFN_Tex2d,id:3157,x:32716,y:31830,ptovrint:False,ptlb:Hatch0,ptin:_Hatch0,varname:node_3157,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:bd148f5837584d6449e7253cc2f7b9fc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4476,x:32716,y:31516,ptovrint:False,ptlb:Hatch1,ptin:_Hatch1,varname:_Hatch1,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:710a06e7e3945ef40ba406a1487b5a5b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:2033,x:32703,y:31193,ptovrint:False,ptlb:Hatch2,ptin:_Hatch2,varname:_Hatch2,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4375680433343934186a89f76580fe6a,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9620,x:32700,y:30881,ptovrint:False,ptlb:Hatch3,ptin:_Hatch3,varname:_Hatch3,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1ecd72a955539e14197246dea5747709,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:4172,x:32700,y:30574,ptovrint:False,ptlb:Hatch4,ptin:_Hatch4,varname:_Hatch4,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4981795baff43ce45bf86b142a79b82c,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Set,id:4247,x:33583,y:32841,varname:LightValue,prsc:2|IN-9500-OUT;n:type:ShaderForge.SFN_Multiply,id:1486,x:32908,y:31773,varname:node_1486,prsc:2|A-1411-OUT,B-3157-RGB;n:type:ShaderForge.SFN_Step,id:178,x:32525,y:31699,varname:node_178,prsc:2|A-8953-OUT,B-7157-OUT;n:type:ShaderForge.SFN_Get,id:7157,x:32335,y:31776,varname:node_7157,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Step,id:2283,x:32525,y:31830,varname:node_2283,prsc:2|A-7157-OUT,B-1056-OUT;n:type:ShaderForge.SFN_Multiply,id:1411,x:32716,y:31699,varname:node_1411,prsc:2|A-178-OUT,B-2283-OUT;n:type:ShaderForge.SFN_Vector1,id:8953,x:32335,y:31635,varname:node_8953,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:1056,x:32335,y:31849,varname:node_1056,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:2253,x:32910,y:31438,varname:node_2253,prsc:2|A-1021-OUT,B-4476-RGB;n:type:ShaderForge.SFN_Multiply,id:1021,x:32718,y:31364,varname:node_1021,prsc:2|A-1998-OUT,B-4928-OUT;n:type:ShaderForge.SFN_Step,id:4928,x:32527,y:31495,varname:node_4928,prsc:2|A-1294-OUT,B-8953-OUT;n:type:ShaderForge.SFN_Step,id:1998,x:32527,y:31364,varname:node_1998,prsc:2|A-4596-OUT,B-1294-OUT;n:type:ShaderForge.SFN_Get,id:1294,x:32337,y:31441,varname:node_1294,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Vector1,id:4596,x:32337,y:31300,varname:node_4596,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Vector1,id:6528,x:32321,y:30987,varname:node_6528,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Get,id:1967,x:32321,y:31128,varname:node_1967,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Step,id:6096,x:32511,y:31051,varname:node_6096,prsc:2|A-6528-OUT,B-1967-OUT;n:type:ShaderForge.SFN_Multiply,id:8,x:32702,y:31051,varname:node_8,prsc:2|A-6096-OUT,B-7422-OUT;n:type:ShaderForge.SFN_Multiply,id:5400,x:32894,y:31125,varname:node_5400,prsc:2|A-8-OUT,B-2033-RGB;n:type:ShaderForge.SFN_Step,id:7422,x:32511,y:31182,varname:node_7422,prsc:2|A-1967-OUT,B-4596-OUT;n:type:ShaderForge.SFN_Vector1,id:4946,x:32319,y:30676,varname:node_4946,prsc:2,v1:-0.1;n:type:ShaderForge.SFN_Get,id:902,x:32319,y:30817,varname:node_902,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Step,id:2805,x:32509,y:30740,varname:node_2805,prsc:2|A-4946-OUT,B-902-OUT;n:type:ShaderForge.SFN_Step,id:9445,x:32509,y:30871,varname:node_9445,prsc:2|A-902-OUT,B-6528-OUT;n:type:ShaderForge.SFN_Multiply,id:2932,x:32700,y:30740,varname:node_2932,prsc:2|A-2805-OUT,B-9445-OUT;n:type:ShaderForge.SFN_Multiply,id:2231,x:32892,y:30814,varname:node_2231,prsc:2|A-2932-OUT,B-9620-RGB;n:type:ShaderForge.SFN_Multiply,id:1067,x:32886,y:30503,varname:node_1067,prsc:2|A-8840-OUT,B-4172-RGB;n:type:ShaderForge.SFN_Multiply,id:8840,x:32694,y:30429,varname:node_8840,prsc:2|A-9030-OUT,B-3792-OUT;n:type:ShaderForge.SFN_Step,id:9030,x:32503,y:30429,varname:node_9030,prsc:2|A-5087-OUT,B-6583-OUT;n:type:ShaderForge.SFN_Step,id:3792,x:32503,y:30560,varname:node_3792,prsc:2|A-6583-OUT,B-4946-OUT;n:type:ShaderForge.SFN_Get,id:6583,x:32313,y:30506,varname:node_6583,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Vector1,id:5087,x:32313,y:30365,varname:node_5087,prsc:2,v1:-0.3;n:type:ShaderForge.SFN_Add,id:3314,x:33415,y:31314,varname:node_3314,prsc:2|A-2408-OUT,B-2253-OUT,C-1486-OUT;n:type:ShaderForge.SFN_Multiply,id:2710,x:32885,y:30148,varname:node_2710,prsc:2|A-3896-OUT,B-8047-RGB;n:type:ShaderForge.SFN_Multiply,id:3896,x:32693,y:30074,varname:node_3896,prsc:2|A-7163-OUT,B-6688-OUT;n:type:ShaderForge.SFN_Step,id:7163,x:32502,y:30074,varname:node_7163,prsc:2|A-8432-OUT,B-5002-OUT;n:type:ShaderForge.SFN_Vector1,id:8432,x:32312,y:30010,varname:node_8432,prsc:2,v1:-1;n:type:ShaderForge.SFN_Step,id:6688,x:32502,y:30210,varname:node_6688,prsc:2|A-5002-OUT,B-5087-OUT;n:type:ShaderForge.SFN_Get,id:5002,x:32291,y:30148,varname:node_5002,prsc:2|IN-4247-OUT;n:type:ShaderForge.SFN_Add,id:2408,x:33170,y:30879,varname:node_2408,prsc:2|A-2710-OUT,B-1067-OUT,C-2231-OUT,D-5400-OUT;n:type:ShaderForge.SFN_LightVector,id:9060,x:33168,y:32130,varname:node_9060,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:4734,x:33168,y:32270,prsc:2,pt:False;n:type:ShaderForge.SFN_Dot,id:9500,x:33366,y:32200,varname:node_9500,prsc:2,dt:0|A-9060-OUT,B-4734-OUT;n:type:ShaderForge.SFN_Multiply,id:2541,x:33612,y:32350,varname:node_2541,prsc:2|A-9500-OUT,B-2015-OUT,C-4880-OUT;n:type:ShaderForge.SFN_LightColor,id:6998,x:33168,y:32427,varname:node_6998,prsc:2;n:type:ShaderForge.SFN_Desaturate,id:2015,x:33366,y:32413,varname:node_2015,prsc:2|COL-6998-RGB,DES-6313-OUT;n:type:ShaderForge.SFN_Vector1,id:6313,x:33168,y:32567,varname:node_6313,prsc:2,v1:1;n:type:ShaderForge.SFN_LightAttenuation,id:4880,x:33366,y:32554,varname:node_4880,prsc:2;n:type:ShaderForge.SFN_Posterize,id:1209,x:33927,y:32284,varname:node_1209,prsc:2|IN-2541-OUT,STPS-6636-OUT;n:type:ShaderForge.SFN_Vector1,id:6636,x:33743,y:32439,varname:node_6636,prsc:2,v1:6;n:type:ShaderForge.SFN_Tex2d,id:8047,x:32693,y:30240,ptovrint:False,ptlb:Hatch5,ptin:_Hatch5,varname:node_8047,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b3a0d63145f56a3468e76303d3412519,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:290,x:33256,y:32849,varname:node_290,prsc:2|A-40-OUT,B-58-OUT;proporder:216-239-3157-4476-2033-9620-4172-8047;pass:END;sub:END;*/

Shader "ShaderExamples/Blinn-Phong Hatching" {
    Properties {
        _Bands ("Bands", Float ) = 5
        _Gloss ("Gloss", Range(0, 1)) = 0.3058287
        _Hatch0 ("Hatch0", 2D) = "white" {}
        _Hatch1 ("Hatch1", 2D) = "white" {}
        _Hatch2 ("Hatch2", 2D) = "white" {}
        _Hatch3 ("Hatch3", 2D) = "white" {}
        _Hatch4 ("Hatch4", 2D) = "white" {}
        _Hatch5 ("Hatch5", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*0.05,1) );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(float3(0,0,0),0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 3.0
            uniform sampler2D _Hatch0; uniform float4 _Hatch0_ST;
            uniform sampler2D _Hatch1; uniform float4 _Hatch1_ST;
            uniform sampler2D _Hatch2; uniform float4 _Hatch2_ST;
            uniform sampler2D _Hatch3; uniform float4 _Hatch3_ST;
            uniform sampler2D _Hatch4; uniform float4 _Hatch4_ST;
            uniform sampler2D _Hatch5; uniform float4 _Hatch5_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
////// Emissive:
                float node_9500 = dot(lightDirection,i.normalDir);
                float LightValue = node_9500;
                float node_5002 = LightValue;
                float node_5087 = (-0.3);
                float4 _Hatch5_var = tex2D(_Hatch5,TRANSFORM_TEX(i.uv0, _Hatch5));
                float node_6583 = LightValue;
                float node_4946 = (-0.1);
                float4 _Hatch4_var = tex2D(_Hatch4,TRANSFORM_TEX(i.uv0, _Hatch4));
                float node_902 = LightValue;
                float node_6528 = 0.1;
                float4 _Hatch3_var = tex2D(_Hatch3,TRANSFORM_TEX(i.uv0, _Hatch3));
                float node_1967 = LightValue;
                float node_4596 = 0.3;
                float4 _Hatch2_var = tex2D(_Hatch2,TRANSFORM_TEX(i.uv0, _Hatch2));
                float node_1294 = LightValue;
                float node_8953 = 0.5;
                float4 _Hatch1_var = tex2D(_Hatch1,TRANSFORM_TEX(i.uv0, _Hatch1));
                float node_7157 = LightValue;
                float4 _Hatch0_var = tex2D(_Hatch0,TRANSFORM_TEX(i.uv0, _Hatch0));
                float3 emissive = ((((step((-1.0),node_5002)*step(node_5002,node_5087))*_Hatch5_var.rgb)+((step(node_5087,node_6583)*step(node_6583,node_4946))*_Hatch4_var.rgb)+((step(node_4946,node_902)*step(node_902,node_6528))*_Hatch3_var.rgb)+((step(node_6528,node_1967)*step(node_1967,node_4596))*_Hatch2_var.rgb))+((step(node_4596,node_1294)*step(node_1294,node_8953))*_Hatch1_var.rgb)+((step(node_8953,node_7157)*step(node_7157,2.0))*_Hatch0_var.rgb));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 3.0
            uniform sampler2D _Hatch0; uniform float4 _Hatch0_ST;
            uniform sampler2D _Hatch1; uniform float4 _Hatch1_ST;
            uniform sampler2D _Hatch2; uniform float4 _Hatch2_ST;
            uniform sampler2D _Hatch3; uniform float4 _Hatch3_ST;
            uniform sampler2D _Hatch4; uniform float4 _Hatch4_ST;
            uniform sampler2D _Hatch5; uniform float4 _Hatch5_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float3 finalColor = 0;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #include "UnityCG.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal d3d11_9x xboxone ps4 psp2 n3ds wiiu 
            #pragma target 3.0
            uniform sampler2D _Hatch0; uniform float4 _Hatch0_ST;
            uniform sampler2D _Hatch1; uniform float4 _Hatch1_ST;
            uniform sampler2D _Hatch2; uniform float4 _Hatch2_ST;
            uniform sampler2D _Hatch3; uniform float4 _Hatch3_ST;
            uniform sampler2D _Hatch4; uniform float4 _Hatch4_ST;
            uniform sampler2D _Hatch5; uniform float4 _Hatch5_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float node_9500 = dot(lightDirection,i.normalDir);
                float LightValue = node_9500;
                float node_5002 = LightValue;
                float node_5087 = (-0.3);
                float4 _Hatch5_var = tex2D(_Hatch5,TRANSFORM_TEX(i.uv0, _Hatch5));
                float node_6583 = LightValue;
                float node_4946 = (-0.1);
                float4 _Hatch4_var = tex2D(_Hatch4,TRANSFORM_TEX(i.uv0, _Hatch4));
                float node_902 = LightValue;
                float node_6528 = 0.1;
                float4 _Hatch3_var = tex2D(_Hatch3,TRANSFORM_TEX(i.uv0, _Hatch3));
                float node_1967 = LightValue;
                float node_4596 = 0.3;
                float4 _Hatch2_var = tex2D(_Hatch2,TRANSFORM_TEX(i.uv0, _Hatch2));
                float node_1294 = LightValue;
                float node_8953 = 0.5;
                float4 _Hatch1_var = tex2D(_Hatch1,TRANSFORM_TEX(i.uv0, _Hatch1));
                float node_7157 = LightValue;
                float4 _Hatch0_var = tex2D(_Hatch0,TRANSFORM_TEX(i.uv0, _Hatch0));
                o.Emission = ((((step((-1.0),node_5002)*step(node_5002,node_5087))*_Hatch5_var.rgb)+((step(node_5087,node_6583)*step(node_6583,node_4946))*_Hatch4_var.rgb)+((step(node_4946,node_902)*step(node_902,node_6528))*_Hatch3_var.rgb)+((step(node_6528,node_1967)*step(node_1967,node_4596))*_Hatch2_var.rgb))+((step(node_4596,node_1294)*step(node_1294,node_8953))*_Hatch1_var.rgb)+((step(node_8953,node_7157)*step(node_7157,2.0))*_Hatch0_var.rgb));
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
