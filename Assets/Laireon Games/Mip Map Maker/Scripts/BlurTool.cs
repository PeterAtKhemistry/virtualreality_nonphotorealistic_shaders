﻿/*
 * The Following Code was developed by Dewald Esterhuizen
 * View Documentation at: http://softwarebydefault.com
 * Licensed under Ms-PL 
  * (Converted for Unity by Laireon Games 2016)
  * (Added edge preservation, and restrained blurring functionality)
*/
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ImageBlurFilter;
using UnityEngine;


public static class BlurTool
{
    public static void ImageBlurFilter(ref Color[] pixels, int width, int height, BlurType blurType)
    {
        switch(blurType)
        {
            case BlurType.Mean3x3:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.Mean3x3, 1.0 / 9.0, 0);
                break;

            case BlurType.Mean5x5:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.Mean5x5, 1.0 / 25.0, 0);
                break;

            case BlurType.Mean7x7:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.Mean7x7, 1.0 / 49.0, 0);
                break;

            case BlurType.Mean9x9:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.Mean9x9, 1.0 / 81.0, 0);
                break;

            case BlurType.GaussianBlur3x3:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.GaussianBlur3x3, 1.0 / 16.0, 0);
                break;

            case BlurType.GaussianBlur5x5:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.GaussianBlur5x5, 1.0 / 159.0, 0);
                break;

            case BlurType.MotionBlur5x5:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur5x5, 1.0 / 10.0, 0);
                break;

            case BlurType.MotionBlur5x5At45Degrees:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur5x5At45Degrees, 1.0 / 5.0, 0);
                break;

            case BlurType.MotionBlur5x5At135Degrees:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur5x5At135Degrees, 1.0 / 5.0, 0);
                break;

            case BlurType.MotionBlur7x7:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur7x7, 1.0 / 14.0, 0);
                break;

            case BlurType.MotionBlur7x7At45Degrees:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur7x7At45Degrees, 1.0 / 7.0, 0);
                break;

            case BlurType.MotionBlur7x7At135Degrees:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur7x7At135Degrees, 1.0 / 7.0, 0);
                break;

            case BlurType.MotionBlur9x9:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur9x9, 1.0 / 18.0, 0);
                break;

            case BlurType.MotionBlur9x9At45Degrees:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur9x9At45Degrees, 1.0 / 9.0, 0);
                break;

            case BlurType.MotionBlur9x9At135Degrees:
                ConvolutionFilter(ref pixels, width, height, BlurMatrix.MotionBlur9x9At135Degrees, 1.0 / 9.0, 0);
                break;
        }
    }


    public static void ImageBlurFilter(this Texture2D sourceBitmap, BlurType blurType)
    {
        switch(blurType)
        {
            case BlurType.Mean3x3:
                sourceBitmap.ConvolutionFilter(BlurMatrix.Mean3x3, 1.0 / 9.0, 0);
                break;

            case BlurType.Mean5x5:
                sourceBitmap.ConvolutionFilter(BlurMatrix.Mean5x5, 1.0 / 25.0, 0);
                break;

            case BlurType.Mean7x7:
                sourceBitmap.ConvolutionFilter(BlurMatrix.Mean7x7, 1.0 / 49.0, 0);
                break;

            case BlurType.Mean9x9:
                sourceBitmap.ConvolutionFilter(BlurMatrix.Mean9x9, 1.0 / 81.0, 0);
                break;

            case BlurType.GaussianBlur3x3:
                sourceBitmap.ConvolutionFilter(BlurMatrix.GaussianBlur3x3, 1.0 / 16.0, 0);
                break;

            case BlurType.GaussianBlur5x5:
                sourceBitmap.ConvolutionFilter(BlurMatrix.GaussianBlur5x5, 1.0 / 159.0, 0);
                break;

            case BlurType.MotionBlur5x5:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur5x5, 1.0 / 10.0, 0);
                break;

            case BlurType.MotionBlur5x5At45Degrees:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur5x5At45Degrees, 1.0 / 5.0, 0);
                break;

            case BlurType.MotionBlur5x5At135Degrees:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur5x5At135Degrees, 1.0 / 5.0, 0);
                break;

            case BlurType.MotionBlur7x7:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur7x7, 1.0 / 14.0, 0);
                break;

            case BlurType.MotionBlur7x7At45Degrees:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur7x7At45Degrees, 1.0 / 7.0, 0);
                break;

            case BlurType.MotionBlur7x7At135Degrees:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur7x7At135Degrees, 1.0 / 7.0, 0);
                break;

            case BlurType.MotionBlur9x9:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur9x9, 1.0 / 18.0, 0);
                break;

            case BlurType.MotionBlur9x9At45Degrees:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur9x9At45Degrees, 1.0 / 9.0, 0);
                break;

            case BlurType.MotionBlur9x9At135Degrees:
                sourceBitmap.ConvolutionFilter(BlurMatrix.MotionBlur9x9At135Degrees, 1.0 / 9.0, 0);
                break;
        }
    }

    private static void ConvolutionFilter(this Texture2D sourceImage, double[,] filterMatrix, double bla, int bias = 0)
    {
        Color[] pixels = sourceImage.GetPixels();
        Color[] output = sourceImage.GetPixels();//grab the pixels, this way we preserve edges that are skipped in blurring

        int filterHeight = Mathf.CeilToInt(filterMatrix.GetLength(0) / 2);
        int filterWidth = Mathf.CeilToInt(filterMatrix.GetLength(1) / 2);

        for(int y = 0; y < sourceImage.height; y++) // Skip top and bottom edges
        {
            for(int x = 0; x < sourceImage.width; x++)  // Skip left and right edges
            {
                double red = 0;//the sum of the pixels
                double green = 0;
                double blue = 0;
                double factor = 0;

                int yPos, xPos, pos;

                if(output[y * sourceImage.width + x].a > 0)//ignore gutters and dont spread the blur!
                    for(int ky = -filterHeight; ky <= filterHeight; ky++)
                        for(int kx = -filterWidth; kx <= filterWidth; kx++)
                        {
                            yPos = y + ky;//we mod to loop the pixels around when we need them

                            if(yPos > 0 && yPos < sourceImage.height)
                            {
                                xPos = x + kx;

                                if(xPos > 0 && xPos < sourceImage.width)
                                {
                                    pos = yPos * sourceImage.width + xPos; // Calculate the adjacent pixel for this kernel point

                                    if(pixels[pos].a > 0)
                                    {
                                        red += filterMatrix[ky + filterHeight, kx + filterWidth] * pixels[pos].r;// Multiply adjacent pixels based on the kernel values
                                        green += filterMatrix[ky + filterHeight, kx + filterWidth] * pixels[pos].g;
                                        blue += filterMatrix[ky + filterHeight, kx + filterWidth] * pixels[pos].b;
                                        factor++;
                                    }
                                }
                            }
                        }

                factor = 1.0 / factor;

                output[y * sourceImage.width + x] = new Color((float)(red * factor), (float)(green * factor), (float)(blue * factor), output[y * sourceImage.width + x].a);
            }
        }

        sourceImage.SetPixels(output);
        sourceImage.Apply();
    }

    static void ConvolutionFilter(ref Color[] pixels, int width, int height, double[,] filterMatrix, double bla, int bias = 0)
    {
        Color[] rawData = (Color[])pixels.Clone();

        int filterHeight = Mathf.CeilToInt(filterMatrix.GetLength(0) / 2);
        int filterWidth = Mathf.CeilToInt(filterMatrix.GetLength(1) / 2);

        for(int y = 0; y < height; y++) // Skip top and bottom edges
        {
            for(int x = 0; x < width; x++)  // Skip left and right edges
            {
                double red = 0;//the sum of the pixels
                double green = 0;
                double blue = 0;
                double factor = 0;

                int yPos, xPos, pos;

                if(pixels[y * width + x].a > 0)//ignore gutters and dont spread the blur!
                    for(int ky = -filterHeight; ky <= filterHeight; ky++)
                        for(int kx = -filterWidth; kx <= filterWidth; kx++)
                        {
                            yPos = y + ky;//we mod to loop the pixels around when we need them

                            if(yPos > 0 && yPos < height)
                            {
                                xPos = x + kx;

                                if(xPos > 0 && xPos < width)
                                {
                                    pos = yPos * width + xPos; // Calculate the adjacent pixel for this kernel point

                                    if(rawData[pos].a > 0)
                                    {
                                        red += filterMatrix[ky + filterHeight, kx + filterWidth] * rawData[pos].r;// Multiply adjacent pixels based on the kernel values
                                        green += filterMatrix[ky + filterHeight, kx + filterWidth] * rawData[pos].g;
                                        blue += filterMatrix[ky + filterHeight, kx + filterWidth] * rawData[pos].b;
                                        factor++;
                                    }
                                }
                            }
                        }

                factor = 1.0 / factor;

                pixels[y * width + x] = new Color((float)(red * factor), (float)(green * factor), (float)(blue * factor), rawData[y * width + x].a);
            }
        }
    }


    public enum BlurType
    {
        None = 0,
        Mean3x3,
        Mean5x5,
        Mean7x7,
        Mean9x9,
        GaussianBlur3x3,
        GaussianBlur5x5,
        MotionBlur5x5,
        MotionBlur5x5At45Degrees,
        MotionBlur5x5At135Degrees,
        MotionBlur7x7,
        MotionBlur7x7At45Degrees,
        MotionBlur7x7At135Degrees,
        MotionBlur9x9,
        MotionBlur9x9At45Degrees,
        MotionBlur9x9At135Degrees,
    }
}

