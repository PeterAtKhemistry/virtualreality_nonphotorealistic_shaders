using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using LaireonFramework;

namespace MipMapMaker
{
    public class DDSAtlasMipMapEditor : EditorWindow
    {
        float previewScale = 0.25f;

        const float PreviewScaleIncrease = 0.05f;//how much to increase scale by as you use the scroll wheel

        public Texture2D[] outputMipMaps, rawMipMaps;
        Color[] debugColours;

        int importIndex = -1;//used to determine if an extra button should be shown

        Texture2D inputTexture, importedCustomMip;

        bool applyDebugColours;


        Vector2 scrollView;

        [MenuItem("Tools/DDS/Editor")]
        static void Init()
        {
            DDSAtlasMipMapEditor window = (DDSAtlasMipMapEditor)GetWindow(typeof(DDSAtlasMipMapEditor), false, "DDS Atlas Mip Map Editor");
            window.Show();
        }

        public void OnGUI()
        {
            GUIHelper.MoveScrollViewsWithMouseClicks(ref scrollView);

            #region Scroll Wheel To Zoom
            if(Event.current.type == EventType.ScrollWheel)
            {
                Event.current.Use();//consumes the standard scrollwheel event so it doesn't affect the scroll view
                previewScale += PreviewScaleIncrease * (Event.current.delta.y > 0 ? -1 : 1);//and scale the preview instead
            }
            #endregion

            scrollView = EditorGUILayout.BeginScrollView(scrollView);

            GUILayout.Space(10);

            EditorGUILayout.HelpBox("Drag your texture below. Ensure its format is either 'Truecolor', 'ARGB 32 bit' or 'RGBA 32 bit'. It will then be converted to DDS with your custom mip maps.", MessageType.Info);

            EditorGUILayout.HelpBox("Also ensure it is read/write enabled! To do this change its texture type to advanced and check the read/write box", MessageType.Info);

            GUILayout.Space(10);

            #region Source Input
            using(new FixedWidthLabel("Input"))
                inputTexture = (Texture2D)EditorGUILayout.ObjectField(inputTexture, typeof(Texture2D), false, GUILayout.MaxWidth(100));

            if(!Application.isPlaying)//dont do this during play mode!
                if(inputTexture != null && inputTexture.mipmapCount > 0 && (outputMipMaps == null || outputMipMaps.Length == 0))
                    GrabMipMapData(ref inputTexture);

            using(new DisableUI(inputTexture == null))
                if(GUILayout.Button("Read file", GUILayout.MaxWidth(100)))
                {
                    GrabMipMapData(ref inputTexture);
                    return;//prevents a null error
                }
            #endregion

            GUILayout.Space(25);

            #region Mip Maps
            if(outputMipMaps != null && outputMipMaps.Length > 0)
            {
                using(new Horizontal())
                {
                    using(new FixedWidthLabel("Preview Scale"))
                        previewScale = EditorGUILayout.Slider(previewScale, 0.05f, 2f, GUILayout.MaxWidth(200));

                    using(new FixedWidthLabel("Colour Debugging"))
                        applyDebugColours = EditorGUILayout.Toggle(GUIContent.none, applyDebugColours);
                }

                using(new Horizontal())
                    for(int i = 0; i < outputMipMaps.Length; i++)
                        if(outputMipMaps[i] != null)
                        {
                            DrawMipMapPreview(i);

                            GUILayout.Space(15);
                        }
            }
            #endregion

            EditorGUILayout.EndScrollView();

            if(GUILayout.Button("Update DDS File"))
                CreateDDSFile();

            Repaint();//force things to update a little more often
        }

        void DrawMipMapPreview(int index)
        {
            int maxWidth = 100;

            if(index == 0)
                maxWidth = 150;//the first image is likely to be much bigger, so we can make the buttons bigger. Needed for the recreate button text

            using(new Vertical())
            {
                using(new FixedWidthLabel("Mip Map " + index)) { }

                if(index == 0)
                    if(GUILayout.Button(new GUIContent("Recreate Mip Maps", "Creates fresh mip maps based on the first texture"), GUILayout.MaxWidth(maxWidth)))
                        RefreshNewMipMaps();

                if(GUILayout.Button(new GUIContent("Export", "Saves this mip map to a file so it can be edited"), GUILayout.MaxWidth(maxWidth)))
                    ExportMipMap(index, false);

                if(importIndex == index)
                    GUI.enabled = false;

                if(GUILayout.Button(new GUIContent("Import", "Loads this mip map from a file"), GUILayout.MaxWidth(maxWidth)))
                {
                    importIndex = index;
                    importedCustomMip = null;
                }

                if(applyDebugColours)
                    using(new FixedWidthLabel("Tint"))
                        debugColours[index] = EditorGUILayout.ColorField(GUIContent.none, debugColours[index], GUILayout.MaxWidth(40));

                GUI.enabled = true;

                if(importIndex == index)
                {
                    using(new Horizontal())
                    {
                        if(GUILayout.Button("X", GUILayout.Width(25)))
                            importIndex = -1;

                        importedCustomMip = (Texture2D)EditorGUILayout.ObjectField(importedCustomMip, typeof(Texture2D), false);
                    }

                    if(importedCustomMip != null)//if the user has loaded in a new texture
                    {
                        try
                        {
                            CopyPixelData(ref rawMipMaps[importIndex], importedCustomMip);//copy the pixel data
                            CopyPixelData(ref outputMipMaps[importIndex], importedCustomMip);

                            importIndex = -1;
                            importedCustomMip = null;//clear the import
                        }
                        catch(System.Exception e)//this will typically be an exception for not enabling read/write on the file
                        {
                            importedCustomMip = null;
                            Debug.LogError(e.Message);
                        }
                    }
                }

                GUIHelper.DrawTexture(outputMipMaps[index], new Rect(0, 0, 1, 1), outputMipMaps[index].width * previewScale, outputMipMaps[index].height * previewScale, Vector2.zero);
            }
        }

        /// <summary>
        /// Basically lets Unity recalculate Mip Mips from the first texture given
        /// </summary>
        void RefreshNewMipMaps()
        {
            ExportMipMap(0, true);//first export the mip map, this means a new texture has been made with mipmaps on
        }

        void CopyPixelData(ref Texture2D destination, Texture2D source)
        {
            destination = new Texture2D(source.width, source.height);
            destination.SetPixels(source.GetPixels());
            destination.Apply();
        }

        void ExportMipMap(int index, bool refreshMipMaps)
        {
            string fileName = FilePath(inputTexture.name + " mip " + index, ".png");

            byte[] data = outputMipMaps[index].EncodeToPNG();

            File.WriteAllBytes(fileName, data);//write the raw file

            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);//make sure Unity knows its there

            TextureImporter textureFile = (TextureImporter)AssetImporter.GetAtPath("Assets/Laireon Games/Mip Map Maker/Resources/Output/" + inputTexture.name + " mip " + index + ".png");//find that asset again, as a Unity file
            textureFile.isReadable = true;//and set it to readable
#if(!UNITY_5_5)
            textureFile.textureFormat = TextureImporterFormat.RGBA32;
#endif
            textureFile.SaveAndReimport();

            if(refreshMipMaps)
            {
                Texture2D mainContent = AssetDatabase.LoadAssetAtPath<Texture2D>(textureFile.assetPath);

                GrabMipMapData(ref mainContent);
            }
            else
                Debug.Log("Saved mip map to: " + fileName);
        }

        void CreateDDSFile()
        {
            if(inputTexture == null)
                Debug.LogError("Your input texture is missing! Assign it above");
            else
            {
                if(applyDebugColours)
                {
                    Color[] pixels;

                    for(int i = 0; i < outputMipMaps.Length; i++)
                    {
                        pixels = outputMipMaps[i].GetPixels();

                        for(int ii = 0; ii < pixels.Length; ii++)
                            pixels[ii] *= debugColours[i];

                        outputMipMaps[i].SetPixels(pixels);
                    }
                }


                string fileName = FilePath(inputTexture.name, ".dds");

                System.Array.Resize<Texture2D>(ref outputMipMaps, 7);

                WriteDDS.CreateFile(fileName, outputMipMaps);

                AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);

                Debug.Log("Created Mip Map: " + fileName);
            }
        }

        void GrabMipMapData(ref Texture2D input)
        {
            try
            {
                outputMipMaps = new Texture2D[input.mipmapCount];
                rawMipMaps = new Texture2D[input.mipmapCount];

                Color32[] pixels;

                for(int i = 0; i < outputMipMaps.Length; i++)
                    if(input.width >> i > 0)//if there are pixels!
                    {
                        pixels = input.GetPixels32(i);//read pixels from the mip map

                        outputMipMaps[i] = new Texture2D(input.width >> i, input.height >> i, input.format, false);
                        outputMipMaps[i].SetPixels32(pixels);
                        outputMipMaps[i].Apply();

                        rawMipMaps[i] = new Texture2D(input.width >> i, input.height >> i, input.format, false);
                        rawMipMaps[i].SetPixels32(pixels);
                        rawMipMaps[i].Apply();
                    }

                while(outputMipMaps[outputMipMaps.Length - 1] == null)
                    System.Array.Resize<Texture2D>(ref outputMipMaps, outputMipMaps.Length - 1);

                while(rawMipMaps[rawMipMaps.Length - 1] == null)
                    System.Array.Resize<Texture2D>(ref rawMipMaps, rawMipMaps.Length - 1);

                #region Default Debug Colours
                if(debugColours == null || debugColours.Length == 0)
                {
                    debugColours = new Color[outputMipMaps.Length];

                    debugColours[0] = Color.white;

                    if(debugColours.Length > 1)
                        debugColours[1] = Color.red;

                    if(debugColours.Length > 2)
                        debugColours[2] = Color.blue;

                    if(debugColours.Length > 3)
                        debugColours[3] = Color.green;

                    if(debugColours.Length > 4)
                        debugColours[4] = Color.grey;

                    if(debugColours.Length > 5)
                        debugColours[5] = Color.yellow;

                    if(debugColours.Length > 6)
                        debugColours[6] = Color.magenta;

                    if(debugColours.Length > 7)
                        debugColours[7] = new Color(255, 185, 0, 255);//orange

                    if(debugColours.Length > 8)
                        debugColours[8] = new Color(128, 82, 33, 255);//brown

                    if(debugColours.Length > 9)
                        debugColours[9] = new Color(255, 124, 246, 255);//pink

                    if(debugColours.Length > 10)
                        debugColours[10] = new Color(150, 255, 109, 255);//lime green

                    if(debugColours.Length > 11)
                        debugColours[11] = new Color(96, 16, 13, 255);//navy blue
                }
                #endregion
                else if(debugColours.Length < outputMipMaps.Length)
                    System.Array.Resize<Color>(ref debugColours, outputMipMaps.Length);
            }
            catch(System.Exception e)//this will typically be an exception for not enabling read/write on the file
            {
                input = null;
                Debug.LogError(e.Message);
            }
        }

        public static string FilePath(string name, string extension)
        {
            if(!Directory.Exists(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath)))
                Directory.CreateDirectory(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath));

            //return string.Format("Assets/Laireon Games/Mip Map Maker/Resources/Output/{0}{1}", name, extension);

            return string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output/{1}{2}", Application.dataPath, name, extension);
        }
    }
}