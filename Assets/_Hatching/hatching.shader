// Shader created with Shader Forge v1.37 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.37;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:35636,y:32828,varname:node_3138,prsc:2|normal-3677-RGB,custl-7196-OUT,olwid-439-OUT,olcol-874-RGB;n:type:ShaderForge.SFN_TexCoord,id:5599,x:32873,y:32050,varname:node_5599,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:1217,x:33317,y:32636,varname:node_1217,prsc:2|A-5021-OUT,B-9060-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9060,x:33130,y:32731,ptovrint:False,ptlb:scale,ptin:_scale,varname:node_9060,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:120;n:type:ShaderForge.SFN_Sin,id:9538,x:33469,y:32636,varname:node_9538,prsc:2|IN-1217-OUT;n:type:ShaderForge.SFN_Abs,id:1988,x:33644,y:32650,varname:node_1988,prsc:2|IN-9538-OUT;n:type:ShaderForge.SFN_NormalVector,id:5749,x:32902,y:32812,prsc:2,pt:True;n:type:ShaderForge.SFN_LightVector,id:866,x:32902,y:33004,varname:node_866,prsc:2;n:type:ShaderForge.SFN_Dot,id:2704,x:33086,y:32913,varname:node_2704,prsc:2,dt:1|A-5749-OUT,B-866-OUT;n:type:ShaderForge.SFN_Smoothstep,id:7306,x:34301,y:32926,varname:node_7306,prsc:2|A-5833-OUT,B-7193-OUT,V-4818-OUT;n:type:ShaderForge.SFN_ComponentMask,id:1125,x:33884,y:32650,varname:node_1125,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-1988-OUT;n:type:ShaderForge.SFN_Slider,id:5887,x:33001,y:33294,ptovrint:False,ptlb:thickness,ptin:_thickness,varname:node_5887,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.3096517,max:10;n:type:ShaderForge.SFN_Step,id:7193,x:34086,y:33075,varname:node_7193,prsc:2|A-5452-OUT,B-1125-G;n:type:ShaderForge.SFN_Multiply,id:5452,x:33593,y:33194,varname:node_5452,prsc:2|A-5887-OUT,B-4818-OUT;n:type:ShaderForge.SFN_Power,id:4818,x:33404,y:32968,varname:node_4818,prsc:2|VAL-2704-OUT,EXP-6456-OUT;n:type:ShaderForge.SFN_Slider,id:6456,x:33030,y:33140,ptovrint:False,ptlb:power,ptin:_power,varname:node_6456,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:4.194985,max:50;n:type:ShaderForge.SFN_Step,id:3605,x:34487,y:32814,varname:node_3605,prsc:2|A-1125-G,B-7306-OUT;n:type:ShaderForge.SFN_Tex2d,id:3677,x:34004,y:32312,ptovrint:False,ptlb:nrm,ptin:_nrm,varname:node_3677,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:True;n:type:ShaderForge.SFN_Vector2,id:122,x:33372,y:32762,varname:node_122,prsc:2,v1:0,v2:0;n:type:ShaderForge.SFN_Vector1,id:5833,x:33404,y:32845,varname:node_5833,prsc:2,v1:0;n:type:ShaderForge.SFN_LightAttenuation,id:7407,x:33697,y:33382,varname:node_7407,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3730,x:34791,y:32937,varname:node_3730,prsc:2|A-3605-OUT,B-4845-OUT,C-8344-OUT;n:type:ShaderForge.SFN_Step,id:4845,x:34264,y:33354,varname:node_4845,prsc:2|A-673-OUT,B-7407-OUT;n:type:ShaderForge.SFN_Slider,id:673,x:33918,y:33466,ptovrint:False,ptlb:shad,ptin:_shad,varname:node_673,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Step,id:7613,x:34022,y:33233,varname:node_7613,prsc:2|A-5452-OUT,B-1125-R;n:type:ShaderForge.SFN_Smoothstep,id:2179,x:34301,y:33171,varname:node_2179,prsc:2|A-5833-OUT,B-7613-OUT,V-4818-OUT;n:type:ShaderForge.SFN_Step,id:8344,x:34487,y:32989,varname:node_8344,prsc:2|A-1125-R,B-2179-OUT;n:type:ShaderForge.SFN_ScreenPos,id:9827,x:32885,y:32319,varname:node_9827,prsc:2,sctp:0;n:type:ShaderForge.SFN_Fresnel,id:9656,x:32646,y:32404,varname:node_9656,prsc:2;n:type:ShaderForge.SFN_Add,id:5021,x:33137,y:32432,varname:node_5021,prsc:2|A-9827-UVOUT,B-9776-OUT;n:type:ShaderForge.SFN_Multiply,id:9776,x:32822,y:32514,varname:node_9776,prsc:2|A-9656-OUT,B-3629-OUT;n:type:ShaderForge.SFN_Slider,id:3629,x:32459,y:32592,ptovrint:False,ptlb:followContour,ptin:_followContour,varname:node_3629,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:9311,x:34819,y:33170,varname:node_9311,prsc:2|A-4641-RGB,B-4211-RGB,T-3730-OUT;n:type:ShaderForge.SFN_Color,id:4211,x:34498,y:33312,ptovrint:False,ptlb:Hi Color,ptin:_HiColor,varname:node_4211,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:4641,x:34714,y:33629,ptovrint:False,ptlb:shadowColor,ptin:_shadowColor,varname:node_4641,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.3276373,c3:0.5220588,c4:1;n:type:ShaderForge.SFN_Slider,id:798,x:35366,y:33582,ptovrint:False,ptlb:outlineWidth,ptin:_outlineWidth,varname:node_798,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2649583,max:1;n:type:ShaderForge.SFN_Color,id:874,x:35332,y:33381,ptovrint:False,ptlb:outlineColor,ptin:_outlineColor,varname:node_874,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1089965,c2:0.2015273,c3:0.3529412,c4:1;n:type:ShaderForge.SFN_Multiply,id:439,x:35596,y:33422,varname:node_439,prsc:2|A-798-OUT,B-7358-OUT;n:type:ShaderForge.SFN_Vector1,id:7358,x:35170,y:33560,varname:node_7358,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Posterize,id:8311,x:33974,y:33637,varname:node_8311,prsc:2|IN-4818-OUT,STPS-5363-OUT;n:type:ShaderForge.SFN_Slider,id:5363,x:33523,y:33720,ptovrint:False,ptlb:colorSteps,ptin:_colorSteps,varname:node_5363,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:5,max:5;n:type:ShaderForge.SFN_Lerp,id:8183,x:34424,y:33663,varname:node_8183,prsc:2|A-934-RGB,B-4211-RGB,T-9587-OUT;n:type:ShaderForge.SFN_Color,id:934,x:34114,y:33895,ptovrint:False,ptlb:LoColor,ptin:_LoColor,varname:node_934,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.06487889,c2:0.1025892,c3:0.1764706,c4:1;n:type:ShaderForge.SFN_Blend,id:7196,x:35041,y:33250,varname:node_7196,prsc:2,blmd:10,clmp:True|SRC-9311-OUT,DST-8183-OUT;n:type:ShaderForge.SFN_Multiply,id:9587,x:34213,y:33663,varname:node_9587,prsc:2|A-8311-OUT,B-4845-OUT;proporder:9060-5887-6456-3677-673-3629-4211-4641-798-874-5363-934;pass:END;sub:END;*/

Shader "Shader Forge/hatching" {
    Properties {
        _scale ("scale", Float ) = 120
        _thickness ("thickness", Range(0, 10)) = 0.3096517
        _power ("power", Range(1, 50)) = 4.194985
        _nrm ("nrm", 2D) = "white" {}
        _shad ("shad", Range(0, 1)) = 1
        _followContour ("followContour", Range(0, 1)) = 1
        _HiColor ("Hi Color", Color) = (1,1,1,1)
        _shadowColor ("shadowColor", Color) = (0,0.3276373,0.5220588,1)
        _outlineWidth ("outlineWidth", Range(0, 1)) = 0.2649583
        _outlineColor ("outlineColor", Color) = (0.1089965,0.2015273,0.3529412,1)
        _colorSteps ("colorSteps", Range(1, 5)) = 5
        _LoColor ("LoColor", Color) = (0.06487889,0.1025892,0.1764706,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float _outlineWidth;
            uniform float4 _outlineColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( float4(v.vertex.xyz + v.normal*(_outlineWidth*0.1),1) );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                return fixed4(_outlineColor.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float _scale;
            uniform float _thickness;
            uniform float _power;
            uniform sampler2D _nrm; uniform float4 _nrm_ST;
            uniform float _shad;
            uniform float _followContour;
            uniform float4 _HiColor;
            uniform float4 _shadowColor;
            uniform float _colorSteps;
            uniform float4 _LoColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _nrm_var = UnpackNormal(tex2D(_nrm,TRANSFORM_TEX(i.uv0, _nrm)));
                float3 normalLocal = _nrm_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_1125 = abs(sin(((i.screenPos.rg+((1.0-max(0,dot(normalDirection, viewDirection)))*_followContour))*_scale))).rg;
                float node_5833 = 0.0;
                float node_4818 = pow(max(0,dot(normalDirection,lightDirection)),_power);
                float node_5452 = (_thickness*node_4818);
                float node_4845 = step(_shad,attenuation);
                float3 finalColor = saturate(( lerp(_LoColor.rgb,_HiColor.rgb,(floor(node_4818 * _colorSteps) / (_colorSteps - 1)*node_4845)) > 0.5 ? (1.0-(1.0-2.0*(lerp(_LoColor.rgb,_HiColor.rgb,(floor(node_4818 * _colorSteps) / (_colorSteps - 1)*node_4845))-0.5))*(1.0-lerp(_shadowColor.rgb,_HiColor.rgb,(step(node_1125.g,smoothstep( node_5833, step(node_5452,node_1125.g), node_4818 ))*node_4845*step(node_1125.r,smoothstep( node_5833, step(node_5452,node_1125.r), node_4818 )))))) : (2.0*lerp(_LoColor.rgb,_HiColor.rgb,(floor(node_4818 * _colorSteps) / (_colorSteps - 1)*node_4845))*lerp(_shadowColor.rgb,_HiColor.rgb,(step(node_1125.g,smoothstep( node_5833, step(node_5452,node_1125.g), node_4818 ))*node_4845*step(node_1125.r,smoothstep( node_5833, step(node_5452,node_1125.r), node_4818 ))))) ));
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float _scale;
            uniform float _thickness;
            uniform float _power;
            uniform sampler2D _nrm; uniform float4 _nrm_ST;
            uniform float _shad;
            uniform float _followContour;
            uniform float4 _HiColor;
            uniform float4 _shadowColor;
            uniform float _colorSteps;
            uniform float4 _LoColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _nrm_var = UnpackNormal(tex2D(_nrm,TRANSFORM_TEX(i.uv0, _nrm)));
                float3 normalLocal = _nrm_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_1125 = abs(sin(((i.screenPos.rg+((1.0-max(0,dot(normalDirection, viewDirection)))*_followContour))*_scale))).rg;
                float node_5833 = 0.0;
                float node_4818 = pow(max(0,dot(normalDirection,lightDirection)),_power);
                float node_5452 = (_thickness*node_4818);
                float node_4845 = step(_shad,attenuation);
                float3 finalColor = saturate(( lerp(_LoColor.rgb,_HiColor.rgb,(floor(node_4818 * _colorSteps) / (_colorSteps - 1)*node_4845)) > 0.5 ? (1.0-(1.0-2.0*(lerp(_LoColor.rgb,_HiColor.rgb,(floor(node_4818 * _colorSteps) / (_colorSteps - 1)*node_4845))-0.5))*(1.0-lerp(_shadowColor.rgb,_HiColor.rgb,(step(node_1125.g,smoothstep( node_5833, step(node_5452,node_1125.g), node_4818 ))*node_4845*step(node_1125.r,smoothstep( node_5833, step(node_5452,node_1125.r), node_4818 )))))) : (2.0*lerp(_LoColor.rgb,_HiColor.rgb,(floor(node_4818 * _colorSteps) / (_colorSteps - 1)*node_4845))*lerp(_shadowColor.rgb,_HiColor.rgb,(step(node_1125.g,smoothstep( node_5833, step(node_5452,node_1125.g), node_4818 ))*node_4845*step(node_1125.r,smoothstep( node_5833, step(node_5452,node_1125.r), node_4818 ))))) ));
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
