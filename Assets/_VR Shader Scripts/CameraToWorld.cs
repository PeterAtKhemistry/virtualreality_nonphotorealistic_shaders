﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraToWorld : MonoBehaviour
{
    Camera _myCamera;

    Camera myCamera
    {
        get
        {
            if (_myCamera == null)
            {
                _myCamera = GetComponent<Camera>();
            }

            return _myCamera;
        }
        set { } }

    // OnPreCull is called before a camera culls the scene.
    void OnPreCull ()
    {
        Shader.SetGlobalMatrix("_Camera2World", myCamera.cameraToWorldMatrix);
	}
}
