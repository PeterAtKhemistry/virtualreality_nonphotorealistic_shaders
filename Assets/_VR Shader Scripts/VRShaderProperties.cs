﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRShaderProperties : MonoBehaviour
{

    Camera myCamera;

    public bool isCentreCamera;

    private void Start()
    {
        myCamera = GetComponent<Camera>();
    }

    // OnPreRender is called before a camera starts rendering the scene.
    void OnPreRender ()
    {
        if(isCentreCamera)
        {
            if(myCamera.stereoTargetEye == StereoTargetEyeMask.Left || myCamera.stereoTargetEye == StereoTargetEyeMask.Right || myCamera.stereoTargetEye == StereoTargetEyeMask.Both)
            {
                Debug.LogWarning("Centre Camera is Targeting Stereo");
            }

            if(myCamera.stereoActiveEye == Camera.MonoOrStereoscopicEye.Mono)
            {
                //Shader.SetGlobalMatrix("_CentreMVPMatrix", myCamera.)
            }
        }

        Shader.SetGlobalVector("_CameraWorldPosition", new Vector4(transform.position.x, transform.position.y, transform.position.z, 0));
        Debug.Log(gameObject.name + " " + transform.position);

		if(myCamera.stereoTargetEye == StereoTargetEyeMask.None)
        {
            Shader.SetGlobalInt("_EyeIndex", 0);
        }
        else if (myCamera.stereoTargetEye == StereoTargetEyeMask.Left)
        {
            Shader.SetGlobalInt("_EyeIndex", 1);
        }
        else if (myCamera.stereoTargetEye == StereoTargetEyeMask.Right)
        {
            Shader.SetGlobalInt("_EyeIndex", 2);
        }
        else if (myCamera.stereoTargetEye == StereoTargetEyeMask.Both)
        {
            if(myCamera.stereoActiveEye == Camera.MonoOrStereoscopicEye.Left)
            {
                Shader.SetGlobalInt("_EyeIndex", 1);
            }
            else if (myCamera.stereoActiveEye == Camera.MonoOrStereoscopicEye.Right)
            {
                Shader.SetGlobalInt("_EyeIndex", 2);
            }
            else if (myCamera.stereoActiveEye == Camera.MonoOrStereoscopicEye.Mono)
            {
                Shader.SetGlobalInt("_EyeIndex", 0);
            }
        }
    }

    private void OnPostRender()
    {
        Shader.SetGlobalInt("_EyeIndex", 0);
    }


}
