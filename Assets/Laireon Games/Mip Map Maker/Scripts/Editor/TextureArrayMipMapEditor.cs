﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using UnityEditor.AnimatedValues;
using System.Collections.Generic;
using LaireonFramework;

namespace MipMapMaker
{
    public class TextureArrayMipMapEditor : EditorWindow
    {
        const string LocalFilePath = "Assets/Laireon Games/Mip Map Maker/Resources/Output/";
        const TextureFormat DefualtTextureFormat = TextureFormat.RGBA32;
        const int MinButtonWidth = 75;

        float previewScale = 0.25f;

        const float PreviewScaleIncrease = 0.1f;//how much to increase scale by as you use the scroll wheel

        Rect ignoreInputPoint = new Rect();//used to help scroll the views with the mouse, but not to scroll whislt dragging a slider

        public Texture2D[][] outputMipMaps, rawMipMaps;
        Color[] debugColours;
        AnimBool[] dropDowns;

        float[][] alphaLevels;
        BlurTool.BlurType[][] blurTypes;

        int importIndex = -1;//used to determine if an extra button should be shown
        int currentMipLevel;
        int[] textureWidths;//an ease of use storage for determining the width and height of mips
        int maxMipMaps = -1;
        bool creatingNewArray;
        string fileName;

        Texture2D importedCustomMip;
        Texture2DArray inputTextureArray;

        List<int> deletedIndexes = new List<int>();

        bool applyDebugColours, skipInputFrame;

        Vector2 scrollView;

        [MenuItem("Tools/Mip Map Editor")]
        static void Init()
        {
            TextureArrayMipMapEditor window = (TextureArrayMipMapEditor)GetWindow(typeof(TextureArrayMipMapEditor), false, "Mip Map Editor");
            window.Show();
        }

        public void OnGUI()
        {
            ProcessUserInput();

            if(skipInputFrame)
            {
                skipInputFrame = false;
                return;//wait until the next layout or draw event before doing anything with the input
            }

            scrollView = EditorGUILayout.BeginScrollView(scrollView);

            GUILayout.Space(10);

            EditorGUILayout.HelpBox("Drag your texture array below and ensure it is read/write enabled! You can also click 'Create New Array' to generate a texture array", MessageType.Info);

            GUILayout.Space(10);

            #region Source Input
            using(new Horizontal())
            {
                using(new FixedWidthLabel(new GUIContent("Input Array"), GUILayout.MaxWidth(200)))
                    inputTextureArray = (Texture2DArray)EditorGUILayout.ObjectField(inputTextureArray, typeof(Texture2DArray), false, GUILayout.MaxWidth(100));

                if(GUILayout.Button("Refresh", GUILayout.MaxWidth(125)))
                    maxMipMaps = -1;//force the editor to read the file again

                if(GUILayout.Button("Create New Array", GUILayout.MaxWidth(125)))
                    creatingNewArray = !creatingNewArray;

                if(creatingNewArray)
                {
                    using(new Vertical(GUILayout.MaxWidth(250)))
                    {
                        using(new FixedWidthLabel("File Name"))
                            fileName = EditorGUILayout.TextField(fileName);

                        EditorGUILayout.HelpBox("Select textures in the project view, then hit create", MessageType.Info);
                    }

                    if(GUILayout.Button("Create", GUILayout.MaxWidth(125)))
                        CreateTextureArray();
                }
            }

            if(inputTextureArray != null)
            {
                ReadData();

                using(new Horizontal())
                {
                    using(new FixedWidthLabel("Mip Map Level"))
                        currentMipLevel = EditorGUILayout.IntSlider(currentMipLevel, 0, maxMipMaps - 1, GUILayout.MaxWidth(200));

                    using(new FixedWidthLabel("Tint Mip Maps"))
                        applyDebugColours = EditorGUILayout.Toggle(GUIContent.none, applyDebugColours);

                    GUILayout.FlexibleSpace();
                }

                using(new Horizontal())
                {
                    using(new FixedWidthLabel("Preview Scale"))
                        previewScale = EditorGUILayout.Slider(previewScale, 0.2f, 10f, GUILayout.MaxWidth(200));

                    previewScale = Mathf.Max(0.2f, previewScale);//don't do below 0.2f

                    if(applyDebugColours)
                        using(new FixedWidthLabel(new GUIContent("Debug Tint", "Applies a tint to this current mip map level")))
                            debugColours[currentMipLevel] = EditorGUILayout.ColorField(GUIContent.none, debugColours[currentMipLevel], GUILayout.MaxWidth(40));

                    GUILayout.FlexibleSpace();
                }

                if(GUILayout.Button(new GUIContent("Recreate Mip Maps", "Creates fresh mip maps based on the first texture"), GUILayout.MaxWidth(150)))
                    CreateNewMipMaps();

                Rect temp = EditorGUILayout.GetControlRect();//grab an area in UI, used to determine current position

                if(temp.y > 0 && ignoreInputPoint.y != temp.y)
                {
                    ignoreInputPoint = temp;
                    ignoreInputPoint.y += 10;//give a little padding
                }

                GUILayout.Space(25);

                if(rawMipMaps != null && maxMipMaps > -1)
                {
                    for(int i = 0; i < inputTextureArray.depth; i++)
                    {
                        if(rawMipMaps[i][currentMipLevel] == null)
                            rawMipMaps[i][currentMipLevel] = ConvertToTexture(inputTextureArray.GetPixels(i, currentMipLevel), textureWidths[currentMipLevel], textureWidths[currentMipLevel]);

                        if(outputMipMaps[i][currentMipLevel] == null)
                            outputMipMaps[i][currentMipLevel] = ConvertToTexture(inputTextureArray.GetPixels(i, currentMipLevel), textureWidths[currentMipLevel], textureWidths[currentMipLevel]);

                        using(new LoopingHorizontal(i, (int)(position.width / Mathf.Max(MinButtonWidth, textureWidths[currentMipLevel] * previewScale)), inputTextureArray.depth - 1, 10, true))
                            DrawMipMapPreview(i);
                    }

                    GUI.color = Color.white;

                    using(new DisableUI(Selection.assetGUIDs.Length == 0))//disbale the button if the user has nothing selected
                    {
                        if(GUILayout.Button("Add Selected Textures", GUILayout.MaxWidth(150)))
                            AddSelectedTextures();
                    }

                    EditorGUILayout.HelpBox("To add new textures first select them in the project view and click the button above", MessageType.Info);
                }
            }
            #endregion

            EditorGUILayout.EndScrollView();

            if(GUILayout.Button("Update Changes"))
                UpdateFile();

            Repaint();//force things to update a little more often
        }

        void ProcessUserInput()
        {
            if(Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.KeypadEnter || (Event.current.keyCode == KeyCode.Return || (Event.current.keyCode == KeyCode.Escape))))//if the user pushed certain keys
                GUIUtility.keyboardControl = 0;//clear the control input

            if(Event.current.type == EventType.MouseDown)
                GUIUtility.keyboardControl = 0;//clear the control input;

            if(Event.current.mousePosition.y > ignoreInputPoint.y || scrollView.y > ignoreInputPoint.y)//only run when the mouse is not in the top section, near the sliders
                GUIHelper.MoveScrollViewsWithMouseClicks(ref scrollView);

            #region Scroll Wheel To Zoom
            if(Event.current.type == EventType.ScrollWheel)
            {
                Event.current.Use();//consumes the standard scrollwheel event so it doesn't affect the scroll view
                previewScale += PreviewScaleIncrease * (Event.current.delta.y > 0 ? -1 : 1);//and scale the preview instead
            }
            #endregion

            if(GUI.GetNameOfFocusedControl().Length == 0)//if nothing is focused
                if(Event.current.type == EventType.KeyDown)
                {
                    switch(Event.current.keyCode)
                    {
                        case KeyCode.Alpha0:
                            //case KeyCode.Keypad0:
                            currentMipLevel = 0;
                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha1:
                            //case KeyCode.Keypad1:
                            if(rawMipMaps != null && rawMipMaps.Length > 1)
                                currentMipLevel = 1;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha2:
                            //case KeyCode.Keypad2:
                            if(rawMipMaps != null && rawMipMaps.Length > 2)
                                currentMipLevel = 2;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha3:
                            //case KeyCode.Keypad3:
                            if(rawMipMaps != null && rawMipMaps.Length > 3)
                                currentMipLevel = 3;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha4:
                            //case KeyCode.Keypad4:
                            if(rawMipMaps != null && rawMipMaps.Length > 4)
                                currentMipLevel = 4;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha5:
                            //case KeyCode.Keypad5:
                            if(rawMipMaps != null && rawMipMaps.Length > 5)
                                currentMipLevel = 5;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha6:
                            //case KeyCode.Keypad6:
                            if(rawMipMaps != null && rawMipMaps.Length > 6)
                                currentMipLevel = 6;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha7:
                            //case KeyCode.Keypad7:
                            if(rawMipMaps != null && rawMipMaps.Length > 7)
                                currentMipLevel = 7;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha8:
                            //case KeyCode.Keypad8:
                            if(rawMipMaps != null && rawMipMaps.Length > 8)
                                currentMipLevel = 8;

                            skipInputFrame = true;
                            break;

                        case KeyCode.Alpha9:
                            //case KeyCode.Keypad9:
                            if(rawMipMaps != null && rawMipMaps.Length > 9)
                                currentMipLevel = 9;

                            skipInputFrame = true;
                            break;
                    }
                }
        }

        void ReadRemainingTextures()
        {
            for(int i = 0; i < inputTextureArray.depth; i++)
                for(int ii = 0; ii < outputMipMaps[i].Length; ii++)
                {
                    if(rawMipMaps[i][ii] == null)
                        rawMipMaps[i][ii] = ConvertToTexture(inputTextureArray.GetPixels(i, ii), textureWidths[ii], textureWidths[ii]);

                    if(outputMipMaps[i][ii] == null)
                        outputMipMaps[i][ii] = ConvertToTexture(inputTextureArray.GetPixels(i, ii), textureWidths[ii], textureWidths[ii]);
                }
        }

        void AddSelectedTextures()
        {
            Texture2D[] sourceImages = ImportSelectedImages();

            if(sourceImages != null)
            {
                Color[] pixels;

                Texture2DArray newOutput = new Texture2DArray(inputTextureArray.width, inputTextureArray.height, inputTextureArray.depth + sourceImages.Length, DefualtTextureFormat, true);

                for(int i = 0; i < inputTextureArray.depth; i++)
                    for(int ii = 0; ii < maxMipMaps; ii++)//read in existing data
                        newOutput.SetPixels(inputTextureArray.GetPixels(i, ii), i, ii);

                for(int i = 0; i < sourceImages.Length; i++)
                    if(sourceImages[i] != null)
                        for(int ii = 0; ii < sourceImages[i].mipmapCount; ii++)
                        {
                            pixels = sourceImages[i].GetPixels(ii);//grab pixels for the current mip leve

                            newOutput.SetPixels(pixels, inputTextureArray.depth + i, ii);//set them into the right place
                        }


                newOutput.Apply(false);

                EditorUtility.CopySerialized(newOutput, inputTextureArray);//copies the changes and updates the existing asset without breaking its project references
                AssetDatabase.SaveAssets();
            }
        }

        /// <summary>
        /// Loops through selected assets and grabs the images
        /// </summary>
        /// <returns></returns>
        Texture2D[] ImportSelectedImages()
        {
            Texture2D[] sourceImages = new Texture2D[Selection.assetGUIDs.Length];

            if(sourceImages.Length == 0)
            {
                Debug.LogError("No textures selected! Select some in your project view");
                return null;
            }

            for(int i = 0; i < Selection.assetGUIDs.Length; i++)
                sourceImages[i] = (Texture2D)AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(Selection.assetGUIDs[i]), typeof(Texture2D));

            int width = sourceImages[0].width;
            int height = sourceImages[0].height;

            for(int i = 1; i < sourceImages.Length; i++)
                if(sourceImages[i].width != width || sourceImages[i].height != height)
                {
                    Debug.LogError("Your textures are not all the same size! Ensure all textures have the same size to prevent errors. You can do this by editing the import settings.");
                    return null;
                }

            return sourceImages;
        }

        void CreateTextureArray()
        {
            Texture2D[] sourceImages = ImportSelectedImages();

            if(sourceImages != null)
            {
                Texture2DArray output = new Texture2DArray(sourceImages[0].width, sourceImages[0].height, sourceImages.Length, TextureFormat.RGBA32, true);
                Color[] pixels;

                for(int i = 0; i < sourceImages.Length; i++)
                    if(sourceImages[i] != null)
                    {
                        pixels = sourceImages[i].GetPixels();

                        output.SetPixels(pixels, i, 0);
                    }

                output.Apply(true);

                string destination = AssetPath(fileName);

                AssetDatabase.CreateAsset(output, destination);
                creatingNewArray = false;

                inputTextureArray = output;
                maxMipMaps = -1;//trigger a refresh

                Debug.Log("File created at: " + destination);
            }
        }

        void ReadData()
        {
            if(Event.current.type == EventType.Layout)
                if(maxMipMaps < 0 || rawMipMaps == null || (rawMipMaps != null && inputTextureArray.depth != rawMipMaps.Length))//if a new run, or a texture with different data has been added
                {
                    CalculateMipMapCount();

                    rawMipMaps = new Texture2D[inputTextureArray.depth][];
                    outputMipMaps = new Texture2D[inputTextureArray.depth][];
                    dropDowns = new AnimBool[inputTextureArray.depth];
                    alphaLevels = new float[inputTextureArray.depth][];
                    blurTypes = new BlurTool.BlurType[inputTextureArray.depth][];
                    debugColours = new Color[maxMipMaps];


                    for(int i = 0; i < rawMipMaps.Length; i++)
                    {
                        rawMipMaps[i] = new Texture2D[maxMipMaps];//make a new array for each texture and its mip maps           
                        outputMipMaps[i] = new Texture2D[maxMipMaps];
                        alphaLevels[i] = new float[maxMipMaps];
                        blurTypes[i] = new BlurTool.BlurType[maxMipMaps];
                        dropDowns[i] = new AnimBool();

                        for(int ii = 0; ii < alphaLevels[i].Length; ii++)
                            alphaLevels[i][ii] = 1;
                    }

                    debugColours[0] = Color.white;

                    if(debugColours.Length > 1)
                        debugColours[1] = Color.red;

                    if(debugColours.Length > 2)
                        debugColours[2] = Color.blue;

                    if(debugColours.Length > 3)
                        debugColours[3] = Color.green;

                    if(debugColours.Length > 4)
                        debugColours[4] = Color.grey;

                    if(debugColours.Length > 5)
                        debugColours[5] = Color.yellow;

                    if(debugColours.Length > 6)
                        debugColours[6] = Color.magenta;

                    if(debugColours.Length > 7)
                        debugColours[7] = new Color(255, 185, 0, 255);//orange

                    if(debugColours.Length > 8)
                        debugColours[8] = new Color(128, 82, 33, 255);//brown

                    if(debugColours.Length > 9)
                        debugColours[9] = new Color(255, 124, 246, 255);//pink

                    if(debugColours.Length > 10)
                        debugColours[10] = new Color(150, 255, 109, 255);//lime green

                    if(debugColours.Length > 11)
                        debugColours[11] = new Color(96, 16, 13, 255);//navy blue

                }
        }

        void UpdateFile()
        {
            ReadRemainingTextures();//ensure all mips have been read into memory
            RemoveDeletedData();

            Color[] pixels;

            for(int i = 0; i < outputMipMaps.Length; i++)
                for(int ii = 0; ii < outputMipMaps[i].Length; ii++)
                {
                    pixels = outputMipMaps[i][ii].GetPixels();

                    if(applyDebugColours && !debugColours[ii].Equals(Color.white))//if there is a colour tint to apply
                        for(int iii = 0; iii < pixels.Length; iii++)
                            pixels[iii] = pixels[iii] * debugColours[ii];//apply it

                    #region Apply Alpha
                    if(alphaLevels[i][ii] < 1)
                        for(int iii = 0; iii < pixels.Length; iii++)
                            pixels[iii] = new Color(pixels[iii].r, pixels[iii].g, pixels[iii].b, pixels[iii].a * alphaLevels[i][ii]);
                    #endregion

                    #region Apply Blurs
                    if(blurTypes[i][ii] != BlurTool.BlurType.None)
                        BlurTool.ImageBlurFilter(ref pixels, outputMipMaps[i][ii].width, outputMipMaps[i][ii].height, blurTypes[i][ii]);//apply any blurring
                    #endregion

                    inputTextureArray.SetPixels(pixels, i, ii);
                }

            inputTextureArray.Apply(false);
            maxMipMaps = -1;//recreate the previews

            if(deletedIndexes.Count > 0)
            {
                AssetDatabase.DeleteAsset(LocalFilePath + inputTextureArray.name + ".Asset");
                AssetDatabase.CreateAsset(inputTextureArray, LocalFilePath + inputTextureArray.name + ".Asset");
                AssetDatabase.Refresh();

                deletedIndexes.Clear();
            }

            Debug.Log("Changes saved to: " + LocalFilePath + inputTextureArray.name);
        }

        /// <summary>
        /// Removes the array elements for the textures marked as deleted
        /// </summary>
        void RemoveDeletedData()
        {
            if(deletedIndexes.Count > 0)
            {
                string name = inputTextureArray.name;
                inputTextureArray = new Texture2DArray(inputTextureArray.width, inputTextureArray.height, outputMipMaps.Length - deletedIndexes.Count, inputTextureArray.format, true);
                inputTextureArray.name = name;

                for(int i = 0; i < deletedIndexes.Count; i++)
                    for(int ii = deletedIndexes[i]; ii < outputMipMaps.Length - 1; ii++)//now loop from the point of deleting
                        outputMipMaps[ii] = outputMipMaps[ii + 1];//scoot the values down to fill the gap

                for(int i = 0; i < deletedIndexes.Count; i++)
                    for(int ii = deletedIndexes[i]; ii < rawMipMaps.Length - 1; ii++)//now loop from the point of deleting
                        rawMipMaps[ii] = rawMipMaps[ii + 1];//scoot the values down to fill the gap

                for(int i = 0; i < deletedIndexes.Count; i++)
                    for(int ii = deletedIndexes[i]; ii < alphaLevels.Length - 1; ii++)//now loop from the point of deleting
                        alphaLevels[ii] = alphaLevels[ii + 1];//scoot the values down to fill the gap

                for(int i = 0; i < deletedIndexes.Count; i++)
                    for(int ii = deletedIndexes[i]; ii < blurTypes.Length - 1; ii++)//now loop from the point of deleting
                        blurTypes[ii] = blurTypes[ii + 1];//scoot the values down to fill the gap

                System.Array.Resize<Texture2D[]>(ref outputMipMaps, outputMipMaps.Length - deletedIndexes.Count);//resize, losing the last values
                System.Array.Resize<Texture2D[]>(ref rawMipMaps, rawMipMaps.Length - deletedIndexes.Count);
                System.Array.Resize<float[]>(ref alphaLevels, alphaLevels.Length - deletedIndexes.Count);
                System.Array.Resize<BlurTool.BlurType[]>(ref blurTypes, blurTypes.Length - deletedIndexes.Count);
            }
        }

        void CalculateMipMapCount()
        {
            if(maxMipMaps < 0)
            {
                maxMipMaps = 0;

                int width = inputTextureArray.width;

                do
                {
                    width /= 2;
                    maxMipMaps++;
                }
                while(width > 0);

                textureWidths = new int[maxMipMaps];
                width = inputTextureArray.width;

                for(int i = 0; i < textureWidths.Length; i++)
                {
                    textureWidths[i] = width;
                    width /= 2;
                }
            }
        }

        void DrawMipMapPreview(int index)
        {
            bool markedForDelete = false;

            if(deletedIndexes.Contains(index))
            {
                markedForDelete = true;
                GUI.color = Color.red;
            }
            else
                GUI.color = Color.white;

            using(new Vertical(GUILayout.MaxWidth(Mathf.Max(MinButtonWidth, rawMipMaps[index][currentMipLevel].width * previewScale))))
            {
                GUIHelper.DrawCenteredToggle(ref dropDowns[index], new GUIContent(dropDowns[index].target ? "Hide" : "Controls"));

                if(EditorGUILayout.BeginFadeGroup(dropDowns[index].faded))
                {
                    #region Import and Export
                    using(new Horizontal())
                    {
                        if(GUILayout.Button(new GUIContent("Export", "Saves this mip map to a file so it can be edited")))
                            ExportMipMap(index);

                        if(importIndex == index)
                            GUI.enabled = false;

                        if(GUILayout.Button(new GUIContent("Import", "Loads this mip map from a file")))
                        {
                            importIndex = index;
                            importedCustomMip = null;
                        }
                    }
                    #endregion

                    if(currentMipLevel == 0)
                        if(GUILayout.Button(new GUIContent("Create New Mips", "Recreates new Mip Maps from this given texture")))
                            CreateNewMipMapIndividual(index);

                    if(!markedForDelete)
                    {
                        if(GUILayout.Button(new GUIContent("Delete", "Marks this texture as needing demoved from the array. Hitting update will delete this texture")))
                            deletedIndexes.Add(index);
                    }
                    else
                        using(new ColourChange(Color.white))
                            if(GUILayout.Button(new GUIContent("Undo Delete", "Restore the texture. If you don't it will be removed next time you update the array")))
                                deletedIndexes.Remove(index);

                    using(new FixedWidthLabel("Alpha"))
                    {
                        GUI.SetNextControlName("alpha");

                        alphaLevels[index][currentMipLevel] = EditorGUILayout.FloatField(alphaLevels[index][currentMipLevel]);
                    }

                    if(alphaLevels[index][currentMipLevel] < 1)
                        if(GUILayout.Button(new GUIContent("Apply To Mips", "Should this value be applied to all mip map levels")))
                            for(int i = 1; i < alphaLevels[index].Length; i++)
                                alphaLevels[index][i] = alphaLevels[index][currentMipLevel];//copy value to all mip levels bar the first

                    using(new FixedWidthLabel("Blur"))
                        blurTypes[index][currentMipLevel] = (BlurTool.BlurType)EditorGUILayout.EnumPopup(blurTypes[index][currentMipLevel]);
                }
                EditorGUILayout.EndFadeGroup();

                GUI.enabled = true;

                if(importIndex == index)
                {
                    using(new Horizontal())
                    {
                        if(GUILayout.Button("X", GUILayout.Width(25)))
                            importIndex = -1;

                        importedCustomMip = (Texture2D)EditorGUILayout.ObjectField(importedCustomMip, typeof(Texture2D), false);
                    }

                    if(importedCustomMip != null)//if the user has loaded in a new texture
                    {
                        try
                        {
                            CopyPixelData(ref rawMipMaps[importIndex][currentMipLevel], importedCustomMip);//copy the pixel data
                            CopyPixelData(ref outputMipMaps[importIndex][currentMipLevel], importedCustomMip);

                            importIndex = -1;
                            importedCustomMip = null;//clear the import
                        }
                        catch(System.Exception e)//this will typically be an exception for not enabling read/write on the file
                        {
                            importedCustomMip = null;
                            Debug.LogError(e.Message);
                        }
                    }
                }

                GUIHelper.DrawTexture(outputMipMaps[index][currentMipLevel], new Rect(0, 0, 1, 1), outputMipMaps[index][currentMipLevel].width * previewScale * Mathf.Pow(2, currentMipLevel), outputMipMaps[index][currentMipLevel].height * previewScale * Mathf.Pow(2, currentMipLevel), Vector2.zero);
                //GUIHelper.DrawTexture(outputMipMaps[index][currentMipLevel], new Rect(0, 0, 1, 1), outputMipMaps[index][currentMipLevel].width * previewScale, outputMipMaps[index][currentMipLevel].height * previewScale, Vector2.zero);
            }
        }

        /// <summary>
        /// Basically lets Unity recalculate Mip Mips from the first texture given
        /// </summary>
        void CreateNewMipMaps()
        {
            for(int i = 0; i < outputMipMaps.Length; i++)
                System.Array.Resize<Texture2D>(ref outputMipMaps[i], 1);//remove all mip map data

            UpdateFile();//save the current data
            inputTextureArray.Apply(true);//recreate mip maps. They will be read on next run
        }

        /// <summary>
        /// Looks at the given top file and creates new mip maps for it. Useful if you import a new texture and dont want to update the entire atlas
        /// </summary>
        /// <param name="index"></param>
        void CreateNewMipMapIndividual(int index)
        {
            Texture2D temp = new Texture2D(rawMipMaps[index][0].width, rawMipMaps[index][0].height, DefualtTextureFormat, true);//make a new texture, and say we want mip maps

            temp.SetPixels(rawMipMaps[index][0].GetPixels());
            temp.Apply();//creates the mip maps

            int width = temp.width;
            int height = temp.height;

            for(int i = 0; i < maxMipMaps; i++)
            {
                CopyPixelData(ref rawMipMaps[index][i], temp, i, width, height);
                CopyPixelData(ref outputMipMaps[index][i], temp, i, width, height);

                width /= 2;
                height /= 2;
            }
        }

        void CopyPixelData(ref Texture2D destination, Texture2D source)
        {
            destination = new Texture2D(source.width, source.height);
            destination.SetPixels(source.GetPixels());
            destination.Apply();
        }

        void CopyPixelData(ref Texture2D destination, Texture2D source, int mipLevel, int width, int height)
        {
            destination = new Texture2D(width, height);
            destination.SetPixels(source.GetPixels(mipLevel));
            destination.Apply();
        }

        void ExportMipMap(int index)
        {
            string fileName = FilePath(inputTextureArray.name + " Texture " + index + " Mip " + currentMipLevel, ".png");

            byte[] data = rawMipMaps[index][currentMipLevel].EncodeToPNG();

            File.WriteAllBytes(fileName, data);//write the raw file

            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);//make sure Unity knows its there

            TextureImporter textureFile = (TextureImporter)AssetImporter.GetAtPath(LocalFilePath + inputTextureArray.name + " Texture " + index + " Mip " + currentMipLevel + ".png");//find that asset again, as a Unity file
            textureFile.isReadable = true;//and set it to readable
            textureFile.SaveAndReimport();

            Debug.Log("Saved mip map to: " + fileName);
        }

        Texture2D ConvertToTexture(Color[] colours, int width, int height)

        {
            Texture2D temp = new Texture2D(width, height);
            temp.SetPixels(colours);
            temp.Apply();

            return temp;
        }

        public static string FilePath(string name, string extension)
        {
            if(!Directory.Exists(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath)))
                Directory.CreateDirectory(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath));

            //return string.Format("Assets/Laireon Games/Mip Map Maker/Resources/Output/{0}{1}", name, extension);

            return string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output/{1}{2}", Application.dataPath, name, extension);
        }

        /// <summary>
        /// this is for creating a file within the resources folder with Unity
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string AssetPath(string name)
        {
            if(!Directory.Exists(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath)))
                Directory.CreateDirectory(string.Format("{0}/Laireon Games/Mip Map Maker/Resources/Output", Application.dataPath));

            return string.Format("Assets/Laireon Games/Mip Map Maker/Resources/Output/{0}.asset", name);
        }
    }
}